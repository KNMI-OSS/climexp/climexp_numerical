FROM ubuntu:22.04
USER root

LABEL maintainer="KNMI <info@knmi.nl>"

# Install system libraries
RUN apt -y update

RUN apt install -y build-essential curl m4

# Install dependencis

# TODO: Triage which of these packages is actually needed

# Scripting
# Have to include this environment variable to prevent this from hanging on a random question
ENV TZ=Europe/Amsterdam
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt install -y \
    python3 ncl-ncarg r-base

# Packages for binaries
RUN apt install -y \
    gfortran libnetcdff-dev libgsl-dev \
    liblapack-dev libfftw3-dev libhdf5-dev
    #  pvm-dev libudunits2-dev   liblapacke-dev 

# Utilities and support for plotting
RUN apt install -y \
    cdo nco netcdf-bin gnuplot-nox grads
    # hdf4-tools gdal-bin 

#RUN apt install -y \
#    libhdf5 \
#    libnetcdf-dev \
#    make \
#    netcdf-fortran \
#    lapack-devel \
#    m4 \
#    python3 \
#    python-pip

# Set up Python config for Fortan->Python coupling
RUN pip install python-config
    
# Set environment
ENV PKG_CONFIG_PATH /build/lib/pkgconfig/
ENV FORTRAN_FLAGS "-I/usr/include -I/build/include/ -I/build/include/fgsl/ -L/build/lib -L/usr/lib64"
ENV CPPFLAGS "-I/usr/include -I/build/include/ -I/build/include/fgsl/ -L/build/lib -L/usr/lib64"
ENV LD_LIBRARY_PATH "/build/lib:$LD_LIBRARY_PATH"


# Install gsl
#WORKDIR /src
#RUN curl -OL "https://ftp.gnu.org/gnu/gsl/gsl-2.5.tar.gz" > gsl-2.5.tar.gz && tar -xzvf gsl-2.5.tar.gz 
#RUN cd /src/gsl-2.5 && ./configure --prefix /build && make -j4 && make install

# Install fgsl
WORKDIR /src
RUN curl -L "https://doku.lrz.de/files/10746505/10746509/1/1684600947933/fgsl-1.4.0.tar.gz" > fgsl.tar.gz && tar -xzvf fgsl.tar.gz 
RUN cd /src/fgsl-1.4.0 && ./configure --prefix /build && make && make install

# Install climate explorer
WORKDIR /src
COPY . climexp
ENV PVM_ARCH build
WORKDIR /src/climexp/${PVM_ARCH}
#RUN cp /build/include/fgsl/fgsl.mod /src/climexp/${PVM_ARCH}/
COPY ./Docker/Makefile.docker /src/climexp/${PVM_ARCH}/Makefile

RUN make

CMD bash
