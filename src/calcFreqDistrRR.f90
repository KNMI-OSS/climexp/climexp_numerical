      subroutine calcFreqDistrRR(a,qc,p75,p95,p99)
! this routine calculates the 75th, 95th and 99th percentiles, averaged over each season,
! in the period calyrbeg, calyrend
!
! only data from wet days (RR > 1.0mm) are entered
!
! here a simple two-parameter Gamma fit is used (rather than the three-parameter or 
! Pearson type III fit)
      implicit none
      include 'comgeneral_f90.h'

      integer       mxlength,mxperc
      parameter     (mxlength=(calyrend-calyrbeg+1)*365,mxperc=5)
      real*8        a(yrbeg:yrend,12,31)
      integer       qc(yrbeg:yrend,12,31)
      real*8        p75(calyrbeg:calyrend+1,nseason)
      real*8        p95(calyrbeg:calyrend+1,nseason)
      real*8        p99(calyrbeg:calyrend+1,nseason)
      real*8        dum(3*365)
      real*8        xdata(mxlength)
      integer       i,j,k,l,ii,jj,kk,length,ndata,nperc,m,nzhang,n
      integer       nmonths,month
      integer       nwl2,absen,nthresh(mxperc)
      real*8        absenr,absentr,percentiles(mxperc),thresh

      common/datablock/xdata
      common/intblock/ndata

      absen = absent + 1
      absenr = dble(absen)
      absentr = dble(absent)

      thresh = 1.0d0

      nperc = 3
      nthresh(1) = 75
      nthresh(2) = 95
      nthresh(3) = 99

      do l=1,nseason
        if (zhang) then
! do the Zhang et al. approach: leave one year out
          do m=calyrbeg,calyrend
            ndata = 0
            do i=calyrbeg,calyrend
              if(m.eq.i) goto 120
              do j=1,nmonths(l)
                call lengthofmonth(1973,month(l,j),length)
                do k=1,length
                  if((qc(i,month(l,j),k).eq.0).and.(a(i,month(l,j),k).gt.thresh)) then
                    ndata = ndata + 1
                    xdata(ndata) = a(i,month(l,j),k)
                  endif
                enddo
              enddo
 120          continue

              ! Use the Zhang et al. (2005) percentile system, linear interpolate between nearest data points
              call zhang2005percRR(nperc,nthresh,percentiles)
              !call poormenpercRR(nperc,nthresh,percentiles)
              
            enddo
            p75(m,l) = percentiles(1)
            p95(m,l) = percentiles(2)
            p99(m,l) = percentiles(3)
          enddo
        endif

! now calculate percentiles over the complete calibration period: no years left out
        m = calyrend+1
        ndata = 0
        do i=calyrbeg,calyrend
          do j=1,nmonths(l)
            call lengthofmonth(1973,month(l,j),length)
            do k=1,length
              if((qc(i,month(l,j),k).eq.0).and.(a(i,month(l,j),k).gt.thresh)) then
                ndata = ndata + 1
                xdata(ndata) = a(i,month(l,j),k)
              endif
            enddo
          enddo
        enddo
        
        if (zhang) then
           call zhang2005percRR(nperc,nthresh,percentiles)
        else
           if(.not.fancy) then
              ! use the "poor men's" percentile estimate: sort data and take the percentiles
              call poormenpercRR(nperc,nthresh,percentiles)
           else
              ! use the "fancy men's" percentile estimate: fit a two-parameter Gamma function
              call gammafitperc(nperc,nthresh,percentiles)
           endif
        endif

        p75(m,l) = percentiles(1)
        p95(m,l) = percentiles(2)
        p99(m,l) = percentiles(3)

      enddo

! if the Zhang et al. approach is NOT followed: then replace p10(calyrbeg:calyrend)
! with the value in p10(calyrend+1)
      if(.not.zhang) then
        do m=calyrbeg,calyrend
          do l=1,nseason
            p75(m,l) = p75(calyrend+1,l)
            p95(m,l) = p95(calyrend+1,l)
            p99(m,l) = p99(calyrend+1,l)
          enddo
        enddo
      endif

!     write(6,123) (p75(calyrend+1,l), l=1,nseason)
!     write(6,123) (p95(calyrend+1,l), l=1,nseason)
!     write(6,123) (p99(calyrend+1,l), l=1,nseason)
 123  format(7e14.4)

      return
      end
!
!---------------------------------------------------------------------------
!
      subroutine poormenpercRR(nperc,nthresh,percentiles)
! this routine calculates the percentile in nthresh of the distribution based
! on the data in the array xdum
!
! here the simple approach is used
      use statistics

      implicit none
      include 'comgeneral_f90.h'

      integer       mxlength,mxperc
      parameter     (mxlength=(calyrend-calyrbeg+1)*365,mxperc=5)
      integer       N,nperc,absen
      real*8        absentr,absenr
      real*8        xdata(mxlength)
      integer       i,j,k,nthresh(mxperc),nthreshd(mxperc)
      real*8        percentiles(nperc)
! NAG things
      integer       ifail,irank(mxlength)
      external      m01daf

      common/datablock/xdata
      common/intblock/N

      absen = absent + 1
      absenr = dble(absen)
      absentr = dble(absent)

      if(N.ge.pdayspresent) then
! sort array xdum into ascending order
        ifail = 0
        !call m01daf(xdata,1,N,'Ascending',irank,ifail)
        call sort_index(xdata,N,irank)

        do j=1,nperc
          nthreshd(j) = nint(dble(nthresh(j)*N)/100.0d0)

           percentiles(j) = xdata(irank(nthreshd(j)))
          
!          do i=1,N
!            if(irank(i).eq.nthreshd(j)) percentiles(j) = xdata(i)
!          enddo
!         write(6,*) j,nthreshd(j),percentiles(j)
        enddo
      else
        do j=1,nperc
          percentiles(j) = absentr
        enddo
      endif

      return
      end
!
!---------------------------------------------------------------------------
!
      subroutine zhang2005percRR(nperc,nthresh,percentiles)
        ! This routine calculates percentiles following Equation 1 in Zhang et al. (2005)
        ! (see https://doi.org/10.1175/JCLI3366.1, based on Frich et al., 2002)
        ! This finds the values either side of the percentile and linearly interpolates them
        !
        ! Inputs:
        ! nperc - number of percentiles to compute (integer) 
        ! nthresh - values of percentages to find (e.g. 10, 90) (integer * nperc) 
        ! Outputs:
        ! percentiles - output percentiles (real*8 * nperc)
        use statistics
  
        implicit none
        include 'comgeneral_f90.h'
  
        integer       mxlength, mxperc
        parameter     (mxlength=(calyrend-calyrbeg+1)*365,mxperc=5)
        integer       N,nperc
        real*8        xdata(mxlength)
        integer       i,j,k,nthresh(mxperc)
        real*8        percentiles(nperc)
        integer       irank(mxlength),absen
        real*8        absenr,absentr
        ! Used in interpolating between the bounding values of a percentile
        real*8        f_percentile, f_to_lower, modified_percentile
        integer       lower_index,upper_index
        real*8        epsilon
        logical       climpact_compare
        real*8        climpact_fact


        common/datablock/xdata
        common/intblock/N
        absen = absent + 1
        absenr = dble(absen)

        ! Introduce a factor to account for machine precision
        epsilon = 4d-16
        climpact_compare = .false.
        climpact_fact = 1d0/3d0

        
        if(N.ge.pdayspresent) then
          ! sort array into ascending order
          call sort_index(xdata,N,irank)
          
          do j=1,nperc
            ! Get the percentile as a fraction
            f_percentile = dble(nthresh(j))/100.0d0

            modified_percentile = f_percentile*N
            ! HACK - Do the same as Climpact, who add a 1/3 factor modifying the calculated percentile
            if (climpact_compare) then
                modified_percentile = climpact_fact + f_percentile * (N + 1 - 2*climpact_fact) - 1
            endif
            
            ! For each percentile requested, get the indices bounding the value requested
            lower_index = floor(modified_percentile + epsilon)

            ! Make sure lower_index is within bounds
            if (lower_index < 0) lower_index = 0
            ! Not N as upper_index needs to be valid too
            if (lower_index > N-1) lower_index = N-1
            ! Set the upper index too
            upper_index = lower_index + 1
            ! Get the fractional distance of the percentile between the upper and lower index
            ! upper_index - lower_index is 1, so dividing is superfluous
            f_to_lower = dble(modified_percentile - lower_index) ! / dble(upper_index - lower_index)

            ! Now calculate the linear interpolation between them  
            ! If f_to_lower is 0, we sit on the lower percentile
            ! If f_to_upper is 1, we sit on the upper percentile
            percentiles(j) = xdata(irank(lower_index))*(1d0-f_to_lower) + & 
                            & xdata(irank(upper_index))*f_to_lower
            
          enddo
        else
          do j=1,nperc
            percentiles(j) = absentr
          enddo
        endif
  
        ! Debug dump of data
        if (percentiles(1) .gt. percentiles(2)) then
          write(*,*) "DEBUG DUMP"
          write(*,*) N, f_to_lower, f_percentile, lower_index, upper_index
          write(*,*) percentiles
          write(*,*) xdata(1:N)
          write(*,*) irank(1:N)
          write(*,*) xdata(irank(1:N))
          call exit(0)
        endif
  
        return
      end subroutine 
!
!---------------------------------------------------------------------------
!
      subroutine gammafitperc(nperc,nthresh,percentiles)
! this routine calculates the percentile of the distribution based
! on the data in the array xdum
!
! here we fit a Gamma distribution to the data using maximum-likelihood
! theory is in Wilks p89
      use statistics

      implicit none
      include 'comgeneral_f90.h'

      integer       mxlength
      parameter     (mxlength=(calyrend-calyrbeg+1)*365)
      integer       N,nperc,absen
      real*8        absentr,absenr
      real*8        xdata(mxlength),array(mxlength)
      integer       i,j,k,nthresh(nperc)
      real*8        percentiles(nperc)
! NAG things
      integer       ifail
      external      g01aaf,g08cgf,g01aef,g01fff
      real*8        g01fff,xmx,xmn

      integer       nclass
      parameter     (nclass=100)
      real*8        xmin,xmax,s2,s3,s4,wtsum,wt(mxlength),chisq
      real*8        eps,t,a,b
      real*8        cint(nclass),par(2),prob(nclass),eval(nclass),ffreq(nclass)
      real*8        chisqi(nclass)
      integer       ifreq(nclass)
      integer       maxcal,ndf
      real*8        xd,x,alpha,beta,D,offset,xmean,p,xthresh
      logical       test

      common/datablock/xdata
      common/intblock/N

      absen = absent + 1
      absenr = dble(absen)
      absentr = dble(absent)

      if(N.ge.pdayspresent) then
        test = .false.

! calculate the mean of the distribution and test for positive skewness
        ifail = 0
!        call g01aaf(N,xdata,0,wt,xmean,s2,s3,s4,xmin,xmax,wtsum,ifail)
        
!        call g01aaf(N,xdata,0,wt,xmean,s2,s3,s4,xmin,xmax,wtsum,ifail)
        call calculate_statistics(xdata,N,xmin,xmean,xmax,s2,s3,s4)

! add a skewness test
!     if(s3.lt.0.0) call error('data should be positively skewed')

        xd = 0.0D0
        do i=1,N
          xd = xd + log(xdata(i))
        enddo
        D = log(xmean) - xd/dble(N)
        alpha = (1.0 + sqrt(1.0 + 4*D/3.0))/(4.0*D)
        beta = xmean/alpha

! this offers the possibility of making a goodness-of-fit test
        if(test) then
          par(1) = alpha
          par(2) = beta

! construct the clas boundaries
          do i=1,nclass
            cint(i) = (i-1)*1.0d0
          enddo

! do the goodness-of-fit test
          ifail = 1
          ! call g08cgf(nclass,ifreq,cint,'G',par,0,prob,chisq,p,ndf,eval,chisqi,ifail)

          ffreq = ifreq
          chisq = chisquare_gamma_evaluate(ffreq,cint,nclass,par)

          write(6,62) chisqi

! if ifail = 10, then we can go on
          if((ifail.ne.0).and.(ifail.ne.10)) then
            write(6,63) ifail
            stop
          endif
        endif

! this calculates the percentiles
        do i=1,nperc
          xthresh = dble(nthresh(i))/100.0d0
          ifail = 0
          !percentiles(i) = g01fff(xthresh,alpha,beta,0.0,ifail)
          percentiles(i) = integrate_gamma_func(xthresh,alpha,beta)
        enddo
      else
        do i=1,nperc
          percentiles(i) = absentr
        enddo
      endif

 62   format('Chi squared: ',e14.4)
 63   format('error in g08cgf- ifail is: ',I4)

      return
      end
!
!---------------------------------------------------------------------------
!
      subroutine fillValue(perc)
! this routine fills any absent value in perc with value derived from
! neighboring values.
!
! the procedure is:
! 1) check if there are absent values at all
! 2) if there is a year in perc with missing values:
!    3) fit a polynomial through the year with the missing data
!    4) evaluate the polynomial on the calender date with the missing data
!
      use pynumerical

      implicit none
      include 'comgeneral_f90.h'

      integer       norder,M
      parameter     (norder=6,M=366)
      integer       absen,i,j,k,length,np,nq,ifail
      integer       npmin,npmax
      real*8        perc(calyrbeg:calyrend+1,12,31)
      real*8        absentr,absenr,xarg,fit
      logical       missing
      real*8        x(M),y(M),w(M),s(norder+1),a(norder+1,norder+1)
      real*8        ak(norder+1),work1(3*M),work2(2*(norder+1))
      !external      e02adf,e02aef

      absen = absent + 1
      absenr = dble(absen)
      absentr = dble(absent)


! check if there is a missing value
      do i=calyrbeg,calyrend+1
        missing  = .false.
        do j=1,12
          call lengthofmonth(i,j,length)
          do k=1,length
            if(perc(i,j,k).lt.absenr) missing = .true.
          enddo
        enddo
!
! if missing = .true., fit polynomial
        if(missing)then
          np = 0
          nq = 0
          do j=1,12
            call lengthofmonth(i,j,length)
            do k=1,length
              np = np + 1
              if(perc(i,j,k).gt.absenr) then
                nq = nq + 1
                x(nq) = dble(np)
                y(nq) = perc(i,j,k)
                w(nq) = 1.0d0
              endif
            enddo
          enddo

! fit polynomial
          ifail = 0
          !call e02adf(nq,norder+1,norder+1,x,y,w,work1,work2,a,s,ifail)
          call pyn_chebyshev_fit(x,y,norder,minval(x),maxval(x))

! fill perc with the evaluation of the polynomial fit
          npmax = np
          npmin = 1
          np = 0
          do j=1,12
            call lengthofmonth(i,j,length)
            do k=1,length
              np = np + 1
              if(perc(i,j,k).lt.absenr) then
                ifail = 0
                xarg = dble(np - npmin)/dble(npmax - npmin)
                !call e02aef(norder+1,ak,xarg,fit,ifail)
                fit = pyn_chebyshev_evaluate(xarg)
                perc(i,j,k) = fit
              endif
            enddo
          enddo
        endif
      enddo

      return
      end
!
!---------------------------------------------------------------------------
!
      integer function nmonths(i)
! this function is used in the percentile calculations for RR
! it returns the number of months in season i (i=1,..,nseason)
!
      implicit none
      include 'comgeneral_f90.h'

      integer       i,j
      integer       sl(nseason)
      data          (sl(j), j=1,nseason) /12,6,6,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1/

      if(i.gt.nseason) call error('out of bounds: function nmonths')

      nmonths = sl(i)

      return
      end
!
!---------------------------------------------------------------------------
!
      integer function month(season,j)
! this function is used in the percentile calculations for RR
! given the season, it returns the month number
!
      implicit none
      include 'comgeneral_f90.h'

      integer       j,season,ndum

      if(season.le.4) then
        if(season.eq.1) then
          month = j
        elseif(season.eq.2)then
          ndum = mod(j+9,12)
          if(ndum.eq.0) ndum = 12
          month = ndum
        elseif(season.eq.3)then
          month = j+3
        else
          ndum = mod(j+11,12)
          if(ndum.eq.0) ndum = 12
          month = ndum
        endif
      else
        if(season.eq.5)then
          month = j+2
        elseif(season.eq.6)then
          month = j+5
        elseif(season.eq.7)then
          month = j+8
        else
          month = season - 7
        endif
      endif

      return
      end 
!
!---------------------------------------------------------------------------
!
