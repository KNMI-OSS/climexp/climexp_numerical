program filterseries

!   Apply hi- or lo-pass filters to a time series in standard
!   format and return the resulting series.

    implicit none
    include 'param.inc'
    include 'getopts.inc'
    integer :: i,j,yr,mo,nmonth,nperyear,mens1,mens
    real :: data(npermax,yrbeg:yrend)
    character :: file*256,line*256,hilo*2,filtertype*12,var*40,units*20
    character :: lvar*200,svar*120,history*50000,metadata(2,100)*2000

    lwrite = .false. 
    if ( command_argument_count() < 4 ) then
        write(0,*) 'usage: filtermonthseries hi|lo filtertype nmon file'
        call exit(-1)
    endif
    call get_command_argument(1,hilo)
    if ( hilo /= 'hi' .and. hilo /= 'lo' ) then
        write(0,*) 'filterseries: error: say hi or lo, not ',hilo
        call exit(-1)
    endif
    call get_command_argument(2,filtertype)
    call get_command_argument(3,line)
    read(line,*,err=901) nmonth
    call get_command_argument(4,file)
    call readseriesmeta(file,data,npermax,yrbeg,yrend,nperyear,var,units, &
        lvar,svar,history,metadata,.false.,lwrite)
    call getopts(5,command_argument_count()-1,nperyear,yrbeg,yrend,.true.,mens1,mens)
    if ( minfac < 0 ) minfac = 0.75
    if ( nperyear == 12 ) then
        write(lvar,'(6a,i4,a)') trim(lvar),' filtered with a ',hilo, &
            '-pass ',trim(filtertype),' filter with cut-off ',nmonth,' months'
    elseif ( nperyear == 4 ) then
        write(lvar,'(6a,i4,2a)') trim(lvar),' filtered with a ',hilo, &
            '-pass ',trim(filtertype),' filter with cut-off ' &
            ,nmonth,' seasons '
    elseif ( nperyear == 360 .or. nperyear == 365 .or. &
        nperyear == 366 ) then
        write(lvar,'(6a,i4,a)') trim(lvar),' filtered with a ',hilo, &
            '-pass ',trim(filtertype),' filter with cut-off ' &
            ,nmonth,' days'
    else
        write(*,'(6a,i4,a)') trim(lvar),' filtered with a ',hilo, &
            '-pass ',trim(filtertype),' filter with cut-off ' &
            ,nmonth,' periods'
    endif

    if ( filtertype == 'running-mean' .or. filtertype == 'box' ) then
        if ( hilo == 'hi' ) then
            call mhipass(data,npermax,nperyear,yrbeg,yrend,nmonth-1,minfac)
        else
            call sumit_minfac(data,npermax,nperyear,yrbeg,yrend,nmonth,'v',minfac,runningmean)
            call shiftseries(data,npermax,nperyear,yrbeg,yrend,nmonth/2)
        endif
    else if ( filtertype == 'loess1' .or. filtertype == 'loess2' ) then
        call myloess(data,npermax,nperyear,yrbeg,yrend,nmonth/2 &
            ,minfac,filtertype,hilo,'month','gaussian',lwrite)
    else
        write(0,*) 'filterseries: error: filtertype ',filtertype,' not yet implemented'
        call exit(-1)
    endif

    call printvar(6,var,units,lvar)
    call printmetadata(6,file,' ',' ',history,metadata)
    call printdatfile(6,data,npermax,nperyear,yrbeg,yrend)

    goto 999
901 write(0,*) 'filterseries: expecting an integer, not ',file
    call exit(-1)
999 continue
end program
