! Test implementation of numpy drop-in replacement for missing numerical routines in Climate Explorer


program testdropins
    ! Note: I don't think chebyshev is ever used, so pynumerical isn't needed
    !use pynumerical
    implicit none

    ! Set up 
    !call pyn_init

    ! Call tests
    !call testchebyshev
    call testminimizer
    call testgamma
    call testchisq
    call testhistogram
    call teststatistics

    ! Clean up the Python numerical module (not really necessary but useful to test)
    !call pyn_cleanup
end program

module testfunctions
    use, intrinsic :: iso_c_binding
    use fgsl
    implicit none

contains
    function testfunction(x, params) bind(c)


        real(c_double), value :: x
        type(c_ptr), value :: params
        real(c_double) :: testfunction

        ! A test output function in x
        testfunction = (x-2d0) * x * (x + 2d0)**2d0

    end function testfunction
end module

subroutine testchebyshev
    use pynumerical    
    implicit none
    
    real :: startTime, stopTime

    real(kind=8) :: output = 0d0

    ! Mock test data
    integer::counter = 0
    integer,parameter::npoints = 40
    real(kind=8)::x_to_fit(npoints)
    real(kind=8)::y_to_fit(npoints)
    integer,parameter::order = 20
    real(kind=8),parameter::range_lower = -1.0
    real(kind=8),parameter::range_upper = 3.0

    ! Analytic solution
    real(kind=8) :: solution_analytic = -4.6875

    ! Start timer
    call cpu_time(startTime)

    x_to_fit = (/(counter, counter=1,npoints)/) / real(npoints)
    y_to_fit = (x_to_fit-2) * x_to_fit * (x_to_fit+2)**2
    call pyn_chebyshev_fit(x_to_fit,y_to_fit,order,range_lower,range_upper)
    output = pyn_chebyshev_evaluate(0.5d0)

    ! End timer and print diagnostics
    call cpu_time(stopTime)

    write(*,*) "~~~"
    write(*,*) "Chebyshev polynomial fit test"
    write(*,*) "Output: ", output
    write(*,*) "Reference solution: ", solution_analytic
    write(*,*) "Error: ", (output - solution_analytic) / solution_analytic * 100d0, " %"
    write(*,*) "Test took ", (stopTime - startTime), " seconds"
    write(*,*) "~~~"

end subroutine testchebyshev

subroutine testminimizer
    use minimizer    
    use testfunctions
    implicit none
    
    real :: startTime, stopTime

    real(kind=8) :: output = 0d0, lower = 1d0, upper = 2d0

    ! Analytic solution
    real(kind=8) :: solution_analytic = (1d0 + sqrt(17d0)) / 4d0

    ! Start timer
    call cpu_time(startTime)

    output = minimize(testfunction,lower,upper)

    ! End timer and print diagnostics
    call cpu_time(stopTime)

    write(*,*) "~~~"
    write(*,*) "Find minimum test"
    write(*,*) "Output: ", output
    write(*,*) "Reference solution: ", solution_analytic
    write(*,*) "Error: ", (output - solution_analytic) / solution_analytic * 100d0, " %"
    write(*,*) "Test took ", (stopTime - startTime), " seconds"
    write(*,*) "~~~"

end subroutine testminimizer

subroutine testgamma
    use statistics    
    use testfunctions
    implicit none
    
    real :: startTime, stopTime

    real(kind=8) :: output = 0d0, x_upper = 1d0, a = 1d0, b = 2d0

    ! Analytic solution
    real(kind=8) :: solution_analytic = 1d0 - sqrt(exp(-1d0))

    ! Start timer
    call cpu_time(startTime)

    ! Run test
    ! For reference, in Python this is equivalent to
    ! scipy.stats.gamma.cdf(1,1,scale=2)
    output = integrate_gamma_func(x_upper,a,b)

    ! End timer and print diagnostics
    call cpu_time(stopTime)

    write(*,*) "~~~"
    write(*,*) "Cumulative Gamma distribution test"
    write(*,*) "Output: ", output
    write(*,*) "Reference solution: ", solution_analytic
    write(*,*) "Error: ", (output - solution_analytic) / solution_analytic * 100d0, " %"
    write(*,*) "Test took ", (stopTime - startTime), " seconds"
    write(*,*) "~~~"

end subroutine testgamma

subroutine testchisq
    use statistics    
    implicit none
    
    real :: startTime, stopTime

    real(kind=8) :: output = 0d0

    ! Mock test data
    integer,parameter::npoints = 10
    integer,parameter::nclasses = 10
    real(kind=8)::observed_values(npoints), gamma_dist_example(npoints), random_values(npoints)
    real(kind=8)::class_boundaries(nclasses-1)
    real(kind=8)::gamma_params(2)
    
    ! Test solution (given by Python function)
    real(kind=8) :: solution_analytic = 0.0011806617251186983

    ! Values generated using Python code:
    class_boundaries = (/ 0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9 /)
    ! Calculated via:
    ! np.diff(scipy.stats.gamma.cdf(np.concatenate(([0],class_boundaries,[1])),1,scale=2)) + 0.01 * (np.random.rand(10) - 0.5)
    gamma_dist_example = (/ 0.04877058, 0.04639201, 0.04412944, 0.04197722, 0.03992997, &
    0.03798256, 0.03613013, 0.03436804, 0.03269189, 0.03109749 /)
    random_values = (/ -0.11840704, -0.03722782, -0.40413629, -0.36294489, -0.17867456, &
    -0.13578492, -0.00388829,  0.36666553,  0.11549322,  0.05435759 /)

    observed_values = gamma_dist_example + 0.01 * random_values
    
    gamma_params = (/ 1d0, 2d0 /)

    ! Start timer
    call cpu_time(startTime)

    ! Run test
    output = chisquare_gamma_evaluate(observed_values,class_boundaries,nclasses,gamma_params)

    ! End timer and print diagnostics
    call cpu_time(stopTime)

    write(*,*) "~~~"
    write(*,*) "Chi-square test test"
    write(*,*) "Output: ", output
    write(*,*) "Reference solution: ", solution_analytic
    write(*,*) "Error: ", (output - solution_analytic) / solution_analytic * 100d0, " %"
    write(*,*) "Test took ", (stopTime - startTime), " seconds"
    write(*,*) "~~~"

end subroutine testchisq

subroutine testhistogram
    use statistics    
    implicit none
    
    real :: startTime, stopTime


    ! Mock test data
    integer,parameter::npoints = 15
    integer,parameter::nclasses = 4
    real(kind=8)::observed_values(npoints)
    real(kind=8)::class_boundaries(nclasses-1)
    integer :: output(nclasses)
    
    ! Test solution (given by Python function)
    integer :: solution_analytic(nclasses)

    ! Test case where I added up values in classes by hand, including ones on boundary
    class_boundaries = (/ 0.25,0.5,0.75 /)
    observed_values = (/ -20.0,10.0,20.0,40.0,100.0,0.1,0.2,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9 /)
    solution_analytic = (/ 4, 3, 2, 6 /)

    ! Start timer
    call cpu_time(startTime)

    ! Run test
    output = make_histogram(observed_values,npoints,class_boundaries,nclasses)

    ! End timer and print diagnostics
    call cpu_time(stopTime)

    write(*,*) "~~~"
    write(*,*) "Histogram test"
    write(*,*) "Output: ", output
    write(*,*) "Reference solution: ", solution_analytic
    write(*,*) "Error: ", sum(real(output - solution_analytic) / real(solution_analytic * nclasses) * 100d0), " %"
    write(*,*) "Test took ", (stopTime - startTime), " seconds"
    write(*,*) "~~~"

end subroutine testhistogram

subroutine teststatistics
    use statistics    
    implicit none
    
    real :: startTime, stopTime

    ! Mock test data
    integer,parameter::npoints = 15
    real(kind=8)::observed_values(npoints)
    real(kind=8)::xmin,xmean,xmax,stddev,skewness,kurtosis
    integer,dimension(npoints) :: indices
    
    ! Test solution (given by Python function)
    real(kind=8)::xmin_ana,xmean_ana,xmax_ana,stddev_ana,skewness_ana,kurtosis_ana

    ! Test cases and solutions calculated with external tool
    observed_values = (/ -20.0,10.0,20.0,40.0,100.0,0.1,0.2,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9 /)
    xmin_ana = -20d0
    xmean_ana = 10.313333333333334d0
    xmax_ana = 100d0
    stddev_ana = 26.965925824187007d0
    skewness_ana = 2.4175543723624244d0
    kurtosis_ana = 5.374914694117118d0

    ! Start timer
    call cpu_time(startTime)

    ! Run test
    call calculate_statistics(observed_values,npoints,xmin,xmean,xmax,stddev,skewness,kurtosis)

    ! End timer and print diagnostics
    call cpu_time(stopTime)

    write(*,*) "~~~"
    write(*,*) "Statistics test"
    write(*,*) "Outputs:", xmin,xmean,xmax,stddev,skewness,kurtosis
    write(*,*) "Error min: ", ((xmin - xmin_ana) / (xmin_ana) * 100d0), " %"
    write(*,*) "Error mean: ", ((xmean - xmean_ana) / (xmean_ana) * 100d0), " %"
    write(*,*) "Error max: ", ((xmax - xmax_ana) / (xmax_ana) * 100d0), " %"
    write(*,*) "Error stddev: ", ((stddev - stddev_ana) / (stddev_ana) * 100d0), " %"
    write(*,*) "Error skewness: ", ((skewness - skewness_ana) / (skewness_ana) * 100d0), " %"
    write(*,*) "Error kurtosis: ", ((kurtosis - kurtosis_ana) / (kurtosis_ana) * 100d0), " %"
    write(*,*) "Test took ", (stopTime - startTime), " seconds"
    write(*,*) "~~~"
    
    ! Test sort
    ! Start timer
    call cpu_time(startTime)

    ! Run test
    call sort_index(observed_values,npoints,indices)

    ! End timer and print diagnostics
    call cpu_time(stopTime)

    write(*,*) "~~~"
    write(*,*) "Sort test"
    write(*,*) "Unsorted input:", observed_values
    write(*,*) "Indices of sorted values:", indices
    write(*,*) "Sorted values:", observed_values(indices)
    write(*,*) "Test took ", (stopTime - startTime), " seconds"
    write(*,*) "~~~"

    
end subroutine teststatistics
