'''
Implementation of numpy drop-in replacement for goodness-of-fit chi squared test function g08cgf 
NOTE - This is not used by the deployed code, but is retained for test comparison purposes
''' 

import scipy
import numpy as np

def evaluate(observed, expected):
    '''
    Evaluate the chisquare test for observed data against the expected data
    '''
    # Normalise expected values
    expected *= np.sum(observed)/np.sum(expected)
    # Run test
    # Note - scipy takes a good second to run for some reason, so replacing with the simpler test below
    #chisq, p_value = scipy.stats.chisquare(observed, expected)
    chisq = np.sum((observed - expected)**2 / expected)
    return chisq

if __name__=="__main__":
    observed = [4.7586510058881426E-002,
                4.6019730464494486E-002,
                4.0088075787753530E-002,
                3.8347770354809962E-002,
                3.8143225501181460E-002,
                3.6624711484264938E-002,
                3.6091247392756112E-002,
                3.8034693516791118E-002,
                3.3846820716077114E-002,
                3.1641066211233082E-002 ]
    class_boundaries = [ 0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9 ]
    expected = np.diff(scipy.stats.gamma.cdf(np.concatenate(([0],class_boundaries,[1])),1,scale=2))
    print(evaluate(observed,expected))
