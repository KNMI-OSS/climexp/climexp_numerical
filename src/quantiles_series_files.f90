program quantiles_series_files

!   compute the data files necessary for the box-and-whisker plot on
!   the right side of the time series plot in the Atlas
!
!   Use a list of files as an input rather than re-computing all the filenames here

    implicit none
    integer,parameter :: yrbeg=1850,yrend=2100,nmodmax=50,nensmax=999,nscenmax=5
    integer :: imod,nmod,iens,nens(nmodmax),iiens(nensmax,nmodmax),i,j &
        ,n,m,yr,iret,ifirst,nperyear,iperiod,mon,ave &
        ,offset,p,yr1r,yr2r,yr1s,yr2s,nperiod,pid,jens
    real :: series(12,yrbeg:yrend,nensmax,nmodmax)
    real :: seriesclim(nensmax,nmodmax),seriesscen(nensmax,nmodmax,9)
    real :: seriesmean(9),seriesquant(-6:6,9), &
        seriesdecvar(9),mean(12)
    real :: s,s1,s2
    logical :: lwrite,lexist,standardunits,lfirst=.true.
    character :: scenarios(nscenmax)*7
    character :: models(nmodmax)*100,shortmodel*20
    character :: string*128,format*20,vars*80,units*30,file*255 &
        ,line*255,var*40,region_subregion*100,season*10 &
        ,oneall*3,files_for_quantiles*1023,quantfile*1023,spid*10 &
        ,rcplist*30,command*1023,filelist*10000
    character :: lvars*120,svars*120,metadata(2,100)*2000,history*50000
    integer :: getpid, iostatus, unit
    data scenarios /'rcp26','rcp45','rcp60','rcp85','sresa1b'/
    lwrite = .false. 
    call getenv('LWRITE',string)
    call tolower(string)
    if ( string == 'true' ) lwrite = .true. 

!   find list of model files

    call get_command_argument(1,var)
    call get_command_argument(2,region_subregion)
    call get_command_argument(3,oneall)
    if ( oneall /= 'one' .and. oneall /= 'all' ) then
        write(0,*) 'usage: quantiles_series var region_subregion '// &
            'one|all mon N ave M yr1r yr2r yr1s yr2s '// &
            'rcplist files_for_quantiles quantfile'
        write(0,*) 'yr1r=yr2r=0 signifies no anomalies'
        call exit(-1)
    end if
    call get_command_argument(4,string)
    if ( string(1:3) /= 'mon' ) then
        write(0,*) 'error: expecting mon'
        call exit(-1)
    end if
    call get_command_argument(5,string)
    read(string,*) mon
    if ( mon < 1 .or. mon > 12 ) then
        write(0,*) 'error: expecting 1 <= mon <= 12, not ',mon
        call exit(-1)
    end if
    call get_command_argument(6,string)
    if ( string(1:3) /= 'ave' ) then
        write(0,*) 'error: expecting ave'
        call exit(-1)
    end if
    call get_command_argument(7,string)
    read(string,*) ave
    if ( ave < 1 .or. ave > 12 ) then
        write(0,*) 'error: expecting 1 <= ave <= 12, not ',ave
        call exit(-1)
    end if
    call get_command_argument(8,string)
    read(string,*) yr1r
    call get_command_argument(9,string)
    read(string,*) yr2r
    call get_command_argument(10,string)
    read(string,*) yr1s
    call get_command_argument(11,string)
    read(string,*) yr2s
    if ( (yr1r < 1800 .and. yr1r /= 0) .or. yr1s < 1800 .or. &
         (yr2r > 2300 .and. yr2r /= 0) .or. yr2s > 2300 ) then
        write(0,*) 'quantiles_series: error: yrs out of range: ',yr1r,yr2r,yr1s,yr2s
        call exit(-1)
    end if
    if ( yr1r > yr2r ) then
        write(0,*) 'quantiles_series: error: yr1r > yr2r: ',yr1r,yr2r
        call exit(-1)
    end if
    if ( yr1s > yr2s ) then
        write(0,*) 'quantiles_series: error: yr1s > yr2s: ',yr1s,yr2s
        call exit(-1)
    end if
    ! rcplist is a list of active scenarios being plotted in the dataset
    ! It's inputted as a string containing the scenario keys separated by underscores
    call get_command_argument(12,rcplist)
    ! inpattern is any pattern to search for, e.g. a list of files with wildcards
    call get_command_argument(13,files_for_quantiles)
    ! quantfile is the output file with the quantiles listed for each scenario
    call get_command_argument(14,quantfile)

    pid = getpid()
    write(spid,'(i10.10)') pid

    write(season,'(a,i2.2,a,i2.2)') 'mon',mon,'ave',ave
    nperiod = 1
    if ( mon+ave-1 > 12 ) then
        offset = -1
    else
        offset = 0
    end if

    ! Clear number arrays
    nmod = 0
    nens = 0
    ! Set a file unit to use
    unit = 1
    iostatus = 0
    ! Empty strings
    history = ''
    metadata = ''
    ! Set series to fill value
    series = 3e33

    ! This contains (as calculated above) a list of all series files to be read for this scenario
    ! The file structure is:
    ! number of models
    ! for each model:
    !   number of ensemble runs
    !   for each ensemble run: the name of the series file to read
    open(unit,file=trim(files_for_quantiles),status='old')
    read(unit,*,iostat=iostatus) nmod
    if (iostatus.ne.0) goto 37707
    do imod=1,nmod
        read(unit,*,iostat=iostatus) nens(imod)
        if (iostatus.ne.0) goto 37707
        do iens=1,nens(imod)
            ! Either use the current series number, or pick the first one if using that option
            jens = iens
            if ( oneall == 'one') jens = 1
            ! Read the series data filename
            read(unit,'(a)',iostat=iostatus) file
            if (iostatus.ne.0) goto 37707
            ! Now read the series into memory
            if ( lwrite ) print *,'reading ',trim(file),'<br>'
            standardunits = .true.
            if ( lfirst ) then
                lfirst = .false.
                ! Get series and metadata
                call readseriesmeta(file,series(1,yrbeg,jens,imod),12, &
                    yrbeg,yrend,nperyear,vars,units,lvars,svars,history,metadata, &
                    standardunits,.false.)
                filelist = file
            else
                ! Just get the series (metadata should be the same as above)
                call readseries(file,series(1,yrbeg,jens,imod),12 &
                    ,yrbeg,yrend,nperyear,vars,units,standardunits,.false.)
                filelist = trim(filelist)//' '//trim(file)
            end if
            ! Get the series as a set of normalised points centred on the mean at zero
            if ( index(var,'rel ') > 0 .and. yr1r > 0 ) then
                ! Subtract the yearly average from the series
                call ensanomalclim(series(1,yrbeg,jens,imod),12 &
                    ,nperyear,yrbeg,yrend,0,0,yr1r,yr2r,mean)
                ! Divide the series by the yearly average
                call takerelanom(series(1,yrbeg,jens,imod),mean, &
                    12,yrbeg,yrend,0,0,nperyear,mon,ave,lwrite)
            end if
        end do 
    end do
    iostatus = 0
    ! Break out of loop in case of I/O error or unexpected EOF
    37707 continue
    ! Close and delete temporary file
    close(unit,status='delete')
    ! Check for IO errors
    if (iostatus.gt.0) then
        write(0,*) "Error reading quantiles file", trim(files_for_quantiles)
        ! Stop with iostatus error code
        stop iostatus
    endif
    if (iostatus.lt.0) then
        write(0,*) "Unexpected EOF in quantiles file", trim(files_for_quantiles)
        ! Stop with generic I/O error
        stop 5 
    endif

    ! If using the 'one' option, only read one ensemble member
    if ( oneall == 'one' ) then
        do imod=1,nmod
            nens(imod) = 1
        end do
    end if
    ! Print diagnostic information
    if ( lwrite ) then
        print *,'metadata'
        do i=1,100
            if ( metadata(1,i) == ' ' ) exit
            print *,trim(metadata(1,i)),' :: ',trim(metadata(2,i))
        end do
        print *,'history :: ',trim(history)
        print *,'files :: ',trim(filelist)
    end if

    ! Now calculate the relevant statistics of the series data

!       compute means

    do imod=1,nmod
        do iens=1,nens(imod)
            call sumit(series(1,yrbeg,iens,imod),12,nperyear,yrbeg,yrend,ave,'v')
        end do
    end do
    seriesclim = 0
    seriesscen = 0
    iperiod = 1
    do imod=1,nmod
        do iens=1,nens(imod)
            n = 0
            if ( yr1r > 0 ) then
                do yr=yr1r+offset,yr2r+offset
                    if ( series(mon,yr,iens,imod) < 1e30 ) then
                        n = n + 1
                        seriesclim(iens,imod) = &
                        seriesclim(iens,imod) &
                        + series(mon,yr,iens,imod)
                    end if
                end do
                if ( n == 0 ) then
                    print *,'weird, n=0 for ',models(imod)
                    do yr=yr1r+offset,yr2r+offset
                        print *,'series(',mon,yr,iens,imod,') = ',series(mon,yr,iens,imod)
                    end do
                else
                    seriesclim(iens,imod) = &
                    seriesclim(iens,imod)/n
                end if
            else
                seriesclim(iens,imod) = 0
            end if
            n = 0
            do yr=yr1s+offset,yr2s+offset
                if ( series(mon,yr,iens,imod) < 1e30 ) then
                    n = n + 1
                    seriesscen(iens,imod,iperiod) = &
                    seriesscen(iens,imod,iperiod) + series(mon,yr,iens,imod)
                end if
            end do
            if ( n == 0 ) then
                if ( lwrite ) print *,'weird, n = 0'
                seriesscen(iens,imod,iperiod) = 3e33
            else
                seriesscen(iens,imod,iperiod) = &
                seriesscen(iens,imod,iperiod)/n - seriesclim(iens,imod)
            end if
        end do
    end do

!       compute s.d. of 20-yr means from inter-ensemble spread

    do iperiod=nperiod,1,-1
        seriesdecvar(iperiod) = 0
        m = 0
        do imod=1,nmod
            if ( nens(imod) > 2 ) then
                m = m + 1
                s = 0
                s1 = 0
                n = 0
                do iens=1,nens(imod)
                    n = n + 1
                    s1 = s1 + seriesscen(iens,imod,iperiod)
                end do
                s1 = s1/n
                s2 = 0
                n = 0
                do iens=1,nens(imod)
                    n = n + 1
                    s2 = s2 + &
                    (seriesscen(iens,imod,iperiod)-s1)**2
                end do
                seriesdecvar(iperiod) = &
                seriesdecvar(iperiod) + s2/(n-1)
            end if
        end do
        if ( m > 0 ) then
            seriesdecvar(iperiod) = &
            seriesdecvar(iperiod)/m
        else
            seriesdecvar(iperiod) = 3e33
        end if
        if ( lwrite ) print *,'seriesdecvar(',iperiod,') = ',seriesdecvar(iperiod)
    end do

!       compute mean & quantiles

    do iperiod=nperiod,1,-1
        call gethistmean(seriesmean(iperiod), &
            seriesscen(1,1,iperiod),nens,nmod,nensmax,lwrite)
        call gethistquant(seriesquant(-6,iperiod), &
            seriesscen(1,1,iperiod),nens,nmod,nensmax,lwrite)
    end do

    open(1,file=trim(quantfile))
    call printmetadata(1,' ',' ','quantiles of '//trim(vars)//' ('//trim(lvars)//')',history,metadata)
    write(1,'(2a)') '# files :: ',trim(filelist)
    write(1,'(2a)') '# year mean min 2.5% 5% 10% 17% 25% ', &
        '50% 75% 83% 90% 95% 97.5% max sd'
    write(1,'(2a)') '# ',trim(rcplist)
    do iperiod=nperiod,1,-1
        write(1,'(i4,50f12.4)') yr2s,seriesmean(iperiod), &
        (seriesquant(i,iperiod),i=-6,6),sqrt(seriesdecvar(1))
    end do
    write(1,'(a)')
    write(1,'(a)')
end program quantiles_series_files

subroutine getensnumber(string,ifirst,n,lwrite)
    implicit none
    integer,intent(in) :: ifirst
    integer,intent(out) :: n
    character,intent(in) :: string*(*)
    logical,intent(in) :: lwrite
    integer :: i,j
    i = index(string,'sresa1b')
    if ( i /= 0 ) then      ! SRES A1b runs
        i = ifirst + index(string(ifirst:),'_144') - 2
        j = i
        if ( string(i-2:i-2) == '_' .and. &
             ichar(string(i:i)) >= ichar('0') .and. &
             ichar(string(i:i)) <= ichar('9') .and. &
             ichar(string(i-1:i-1)) >= ichar('0') .and. &
             ichar(string(i-1:i-1)) <= ichar('9') ) then
            i = i-3
        end if
        if ( j /= i ) then
            read(string(i+2:j),*) n
        else
            n = 0
        end if
        n = n + 1
    else                    ! RCP runs
        i = index(string,'_r', .true. )
        if ( i == 0 ) then
            write(0,*) 'error: cannot find "_r" in ',trim(string)
            n = 0
            return
        end if
        i = i+2
        j = i + index(string(i:),'i') - 2
!!!     print *,'searching for ensemble number in ',trim(string),i,j,string(i:j)
        if ( j < i ) then
            write(0,*) 'getensnumber: error: cannot find "i" in ',trim(string(i:))
            call exit(-1)
        end if
        read(string(i:j),*) n
    end if
    if ( lwrite ) print *,'found ensemble number ',n,' in ',trim(string)
end subroutine getensnumber

subroutine gethistmean(mean,data,nens,nmod,nensmax,lwrite)

!   Compute the mean of a set of values.
!   All models are weighted equally

    implicit none
    integer,intent(in) :: nmod,nensmax
    integer,intent(in) :: nens(nmod)
    real,intent(in) :: data(nensmax,nmod)
    real,intent(out) :: mean
    logical,intent(in) :: lwrite
    integer :: iens,imod,nmodmax
    real :: s

    if ( lwrite ) then
        print *,'gethistmean: input'
        do imod=1,nmod
            print '(i3,100f7.3)',imod,(data(iens,imod),iens=1,nens(imod))
        end do
    end if
    s = 0
    do imod=1,nmod
        do iens=1,nens(imod)
            if ( data(iens,imod) > 100 ) then
                write(0,*) 'gethistmean: warning: data(',iens,imod,') = ',data(iens,imod)
            end if
            s = s + data(iens,imod)/nens(imod)
        end do
    end do
    mean = s/nmod
    if ( lwrite ) print '(a,f7.3)','mean = ',mean
end subroutine gethistmean

subroutine gethistquant(quant,data,nens,nmod,nensmax,lwrite)

!   Compute some qunatiles of a set of values:
!   min, 5%, 25%, 50%, 75%, 95%, max
!   All models are weighted equally

    implicit none
    integer,intent(in) :: nmod,nensmax
    integer,intent(in) :: nens(nmod)
    real,intent(in) :: data(nensmax,nmod)
    real,intent(out) :: quant(-6:6)
    logical,intent(in) :: lwrite
    integer :: iens,imod,i,n,nnens(1024),nmodmax
    real :: point(1024)

!   just convert the data to the same format as quantiles_field uses

    n = 0
    do imod=1,nmod
        do iens=1,nens(imod)
            n = n + 1
            if ( n > 1024 ) then
                write(0,*) 'gethistquant: error: array too small ',1024
                call exit(-1)
            end if
            point(n) = data(iens,imod)
            nnens(n) = nens(imod)
        end do
    end do

    call getweightedquant(point,nnens,n,nmod,quant,lwrite)

end subroutine gethistquant
