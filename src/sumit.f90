subroutine sumit(data,npermax,nperyear,yrbeg,yrend,lsum,oper)
    implicit none
    integer :: npermax,nperyear,yrbeg,yrend,lsum
    real :: data(npermax,yrbeg:yrend)
    integer :: runningmean_direction
    character(1) :: oper
    real :: minfac

    ! Default to forward mean, use sumit_directional to set directional means
    runningmean_direction = 1
    call sumit_directional(data,npermax,nperyear,yrbeg,yrend,lsum,oper,minfac,runningmean_direction)
    
end subroutine sumit

subroutine sumit_directional(data,npermax,nperyear,yrbeg,yrend,lsum,oper,minfac,runningmean_direction)
  implicit none
  integer :: npermax,nperyear,yrbeg,yrend,lsum
  real :: data(npermax,yrbeg:yrend)
  integer,intent(in) :: runningmean_direction
  character(1) :: oper
  real :: minfac
    
  minfac=0.9999
  call sumit_minfac(data,npermax,nperyear,yrbeg,yrend,lsum,oper,minfac,runningmean_direction)
end subroutine sumit_directional

subroutine sumit_minfac(data,npermax,nperyear,yrbeg,yrend,lsum,oper,minfac,runningmean_direction)

!   sum lsum consecutive months/days/... into the first one

    implicit none
    integer :: npermax,nperyear,yrbeg,yrend,lsum
    real :: data(npermax,yrbeg:yrend),newdata(npermax,yrbeg:yrend)
    real :: minfac
    ! Running mean direction
    ! -1 = backwards
    ! 0 = centred
    ! 1 = forwards
    integer,intent(in) :: runningmean_direction
    character(1) :: oper
    integer :: i,j,m,n,ii,ns,nfirst,nlast
    real :: s
    real,parameter :: absent=3e33
    logical,parameter :: lwrite=.FALSE.
    integer,external :: leap

    newdata = data
    ! Default to forward, e.g.
    ! 0,1,2,3,4 for lsum=5
    ! 0,1,2,3   for lsum=4
    nfirst = 0
    nlast = lsum-1
    ! backward, e.g. 
    ! -4,-3,-2,-1,0 for lsum=5
    ! -3,-2,-1,0    for lsum=4
    if (runningmean_direction == -1) then
        nfirst = -lsum+1
        nlast = 0
    ! centred (always "rounds up" for centre if lsum is even)
    ! -2,-1,0,1,2 for lsum=5
    ! -1,0,1,2    for lsum=4
    elseif (runningmean_direction == 0) then
        nfirst = -int((lsum-1)/2)
        nlast = nfirst + lsum-1
    ! forward
    else
        ! Just use defaults (forward)
    endif

    if ( lsum > 1 ) then
        if ( lwrite ) then
            print *,'sumit: npermax,nperyear = ',npermax,nperyear
            print *,'       yrbeg,yrend      = ',yrbeg,yrend
            print *,'       lsum,oper        = ',lsum,oper
            print *,'       data(1990)       = ',(data(i,1990),i=1,nperyear)
            print *,'       data(1991)       = ',(data(i,1991),i=1,nperyear)
        endif
        do i=yrbeg,yrend
            do j=1,nperyear
                s = 0
                ns = 0
                if ( data(j,i) < 0.9*absent ) then
                    do n=nfirst,nlast
                        m = j+n
                        if ( nperyear == 366 .and. leap(i) == 1 .and. j < 60 .and. m >= 60 ) m = m + 1
                        ! Correct month so that it is between 1 and nperyear, and adjust the year to match
                        ! e.g. 13th month of 2010 becomes 1st month of 2011
                        call normon(m,i,ii,nperyear)
                        if ( ii < yrbeg ) cycle
                        if ( ii > yrend ) cycle
                        if ( data(m,ii) < 0.9*absent ) then
                            ns = ns + 1
                            if ( oper == '+' .or. oper == 'v' ) then
                                s = s + data(m,ii)
                            elseif ( oper == 'a' ) then
                                s = max(s,data(m,ii))
                            elseif ( oper == 'i' ) then
                                s = min(s,data(m,ii))
                            else
                                print *,'sumit: error: cannot handle oper = ',oper,', only [+vai]'
                                call exit(-1)
                            endif
                        endif
                    enddo
                    if ( ns < minfac*lsum ) then
                        s = absent
                    else if ( oper == 'v' .and. s < 0.9*absent ) then
                        s = s/lsum
                    endif
                    newdata(j,i) = s
                endif
            enddo
        enddo
        ! Copy buffered data to output
        data = newdata
        if ( lwrite ) then
            print *,'sumit: after averaging'
            print *,'       data(1990)       = ',(data(i,1990),i=1,nperyear)
            print *,'       data(1991)       = ',(data(i,1991),i=1,nperyear)
        endif
    endif

end subroutine sumit_minfac
