subroutine normsd(data,npermax,nperyear,yrbeg,yrend,yr1,yr2)
    implicit none
    integer,intent(in) :: npermax,nperyear,yrbeg,yrend,yr1,yr2
    real,intent(inout) :: data(npermax,yrbeg:yrend)
    call ensnormsd(data,npermax,nperyear,yrbeg,yrend,0,0,yr1,yr2)
end subroutine normsd

subroutine ensnormsd(data,npermax,nperyear,yrbeg,yrend,nens1,nens2,yr1,yr2)

!   normalize data to its (monthly) standard deviation

    implicit none
    integer,intent(in) :: npermax,nperyear,yrbeg,yrend,yr1,yr2,nens1,nens2
    real,intent(inout) :: data(npermax,yrbeg:yrend,0:nens2)
    integer :: i,j,n,iens
    real :: x1,x2

    ! Run calculation separately for each period in a year
    do j=1,nperyear
        x1 = 0
        x2 = 0
        n = 0
        ! Calculate sum of data and number of valid points
        do iens=nens2,nens2
            do i=yr1,yr2
                if ( data(j,i,iens) < 1e30 ) then
                    n = n + 1
                    x1 = x1 + data(j,i,iens)
                end if
            end do
        end do
        ! If less than 2 valid points, calculation is impossible
        ! Set the data to the fill value and return
        if ( n < 2 ) then
            do iens=nens1,nens2
                do i=yrbeg,yrend
                    data(j,i,iens) = 3e33
                end do
            end do
        else
            ! Calculate mean
            x1 = x1/n
            ! Calculate SUM{(data - mean)**2}
            do iens=nens1,nens2
                do i=yr1,yr2
                    if ( data(j,i,iens) < 1e30 ) then
                        x2 = x2 + (data(j,i,iens)-x1)**2
                    end if
                end do
            end do
            ! Calculate std dev as
            ! SQRT(SUM{(data - mean)**2}/(N-1))
            ! (corrected sample std dev)
            x2 = sqrt(x2/(n-1))
            ! Now normalise each period of the year by the calculated std dev for that period
            if ( x2 > 0 ) then
                do iens=nens1,nens2
                    do i=yrbeg,yrend
                        if ( data(j,i,iens) < 1e30 ) then
                            data(j,i,iens) = (data(j,i,iens)-x1)/x2
                        end if
                    end do
                end do
            else
                ! If no value std dev, set data to fill value
                do iens=nens1,nens2
                    do i=yrbeg,yrend
                        data(j,i,iens) = 3e33
                    end do
                end do
            end if
        end if
    end do
end subroutine ensnormsd
