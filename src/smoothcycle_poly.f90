! Pulled out of indexSupport.f as it needs to compile under Fortran 90 to allow module drop-in
! SO this doesn't seem to be used anywhere else in the code...

!
!---------------------------------------------------------------------------
!
      subroutine smoothcycle_poly(xcycle)
        ! this routine smooth the annual cycle by fitting a polynomial approximation
        ! to the annual cycle
        !
              
              use pynumerical

              implicit none
              include 'comgeneral_f90.h'
        
              integer       ifail,kplus1,kp1max,nrows,mmax
              parameter     (kp1max=25,nrows=kp1max,mmax=12*31*3)
              real*8        xcycle(12,31)
              real*8        xdum(mmax),ydum(mmax)
              real*8        a(nrows,kp1max),ak(kp1max),s(kp1max),w(mmax)
              real*8        work1(3*mmax),work2(2*kp1max)
              integer       i,j,k,absen,nrec,np,length,nrecmax
              real*8        absenr,absentr,x,fit
              external      e02adf,e02aef
        
              absen = absent + 1
              absenr = dble(absen)
              absentr = dble(absent)
        
              nrec = 0
              np = 0
        
        ! make an array of 14 months
              do k=1,31
                nrec = nrec + 1
                if(xcycle(12,k).gt.absenr) then
                  np=np+1
                  xdum(np) = dble(nrec)
                  ydum(np) = xcycle(12,k)
                  w(np) = 1.0
                endif
              enddo
              do j=1,12
                call lengthofmonth(1973,j,length)
                do k=1,length
                  nrec = nrec + 1
                  if(xcycle(j,k).gt.absenr) then
                    np=np+1
                    xdum(np) = dble(nrec)
                    ydum(np) = xcycle(j,k)
                    w(np) = 1.0
                  endif
                enddo
              enddo
              do k=1,31
                nrec = nrec + 1
                if(xcycle(1,k).gt.absenr) then
                  np=np+1
                  xdum(np) = dble(nrec)
                  ydum(np) = xcycle(1,k)
                  w(np) = 1.0
                endif
              enddo
        
              nrecmax = nrec
        
              if(np.ge.25) then
        ! this makes the fit with Chebyshev polynomials
                kplus1=25
                ifail = 0
                !call e02adf(np,kplus1,nrows,xdum,ydum,w,work1,work2,a,s,ifail)
                !call e02adf(nq,norder+1,norder+1,x,y,w,work1,work2,a,s,ifail)
                ! This saves the result of the polynomial internally
                call pyn_chebyshev_fit(xdum,ydum,kplus1-1,minval(xdum),maxval(ydum))
        
                do j=1,kplus1
                  ak(j) = a(kplus1,j)
                enddo
        
        ! this calculates the smoothed cycled by evaluating the fit
                nrec = 31
                do j=1,12
                  call lengthofmonth(1973,j,length)
                  do k=1,length
                    nrec = nrec + 1
                    x = dble((nrec-1)-(nrecmax-nrec))/dble(nrecmax-1)
                    ifail = 0
                    !call e02aef(kplus1,ak,x,fit,ifail)
                    fit = pyn_chebyshev_evaluate(x)
                    xcycle(j,k) = fit
                  enddo
                enddo
        
        ! add leap year
                xcycle(2,29) = (xcycle(2,28) + xcycle(3,1))/2.0d0
              endif
        
              return
              end