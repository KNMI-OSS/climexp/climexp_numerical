! Fortran-only functions to provide open-source statistics support in Climate Explorer

module statistics

    contains

function integrate_gamma_func(x_upper,a,b)
    ! Return integral of the probability distribution Gamma(a,b) between -inf and x_upper

    use, intrinsic :: iso_c_binding
    use fgsl

    implicit none

    ! Maximum value to integrate up to
    real(fgsl_double),intent(in)::x_upper 
    ! Parameters of the Gamma function
    real(fgsl_double),intent(in)::a,b

    ! Output
    real(kind=8)::integrate_gamma_func

    integrate_gamma_func = fgsl_cdf_gamma_p(x_upper,a,b)

end function integrate_gamma_func

subroutine sort_index(xdata,ndata,indices)
    ! Take an array of unsorted values xdata of size ndata and calculate the indices that would sort the array 

    use, intrinsic :: iso_c_binding
    use fgsl

    implicit none
    
    integer :: ndata
    integer :: indices(:)

    real(fgsl_double), target :: xdata(:)
    integer(fgsl_size_t) :: ndata_fgsl
    integer(fgsl_size_t) :: indices_fgsl(ndata)

    ndata_fgsl = ndata
    call fgsl_sort_index(indices_fgsl,xdata,1_fgsl_size_t,ndata_fgsl)
    ! GSL uses zero-ordered indices
    indices = indices_fgsl+1

end subroutine sort_index


function chisquare_gamma_evaluate(observed_values,class_boundaries,nclasses,gamma_params)
    ! Evaluate chi-square test using the Gamma probability distribution as a test distribution

    ! Inputs ==>
    ! Input value(s) to evaluate polynomial at (double), size nclasses
    real(kind=8),intent(in) :: observed_values(:)
    ! Values of class boundaries (not including extremes of 0 and 1), size nclasses-1
    real(kind=8),intent(in) :: class_boundaries(:)
    ! Number of classes to test against (i.e. values in the observed and estimated arrays)
    integer,intent(in) :: nclasses
    ! Parameters for the Gamma distribution a and b for distribution Gamma(a,b)
    real(kind=8),intent(in) :: gamma_params(2)
    ! Outputs ==>
    ! Value at x_to_fit to output
    real(kind=8) :: chisquare_gamma_evaluate
    ! Internal ==>
    ! Calculated expected values for the given distribution
    real(kind=8) :: expected_values(nclasses)
    ! Counter, status value
    integer :: icount, ierror
    
    ! Calculate expected values
    ! Loop through the class bounds to calculate portion of the Gamma function inside the class
    ! Handle boundaries at 0 and 1 separately as class boundaries array doesn't include them
    do icount=1,nclasses-1
        expected_values(icount) = integrate_gamma_func(class_boundaries(icount),gamma_params(1),gamma_params(2))
    enddo
    expected_values(nclasses) = integrate_gamma_func(1d0,gamma_params(1),gamma_params(2))
    do icount=nclasses,2,-1
        expected_values(icount) = expected_values(icount) - expected_values(icount-1)
    enddo

    ! Normalise expected values to match total frequency in observed values
    expected_values = expected_values * sum(observed_values)/sum(expected_values)
    ! Run test
    chisquare_gamma_evaluate = sum((observed_values - expected_values)**2 / expected_values)
    
end function chisquare_gamma_evaluate

function make_histogram(observations,nobservations,class_boundaries,nclasses)
    ! Create a histogram / frequency table for a set of observations of a variable

    ! Inputs ==>
    ! Input values to put into the histogram (size nobservations)
    real(kind=8),intent(in) :: observations(:)
    integer,intent(in) :: nobservations
    ! Boundaries to set the histogram points (size nclasses-1)
    real(kind=8),intent(in) :: class_boundaries(:)
    integer,intent(in) :: nclasses
    ! Outputs ==>
    ! Value at x_to_fit to output
    integer :: make_histogram(nclasses)
    ! Internal ==>
    integer :: iclass, iobservation
    real(kind=8) :: lower,upper

    ! Just in case the compiler doesn't do it, empty the resulting histogram
    make_histogram = 0

    ! Begin with a very simple O(N^2) loop, can be optimised if needed
    ! Search through each element in the histogram between the class boundaries
    do iclass=1,nclasses
        ! Set lower bound to search inside
        if (iclass.eq.1) then
           lower = minval(observations) - 1d0
        else 
            lower = class_boundaries(iclass-1) 
        end if
        ! Set upper bound to search inside
        if (iclass.eq.nclasses) then
            upper = maxval(observations) + 1d0
        else 
            upper = class_boundaries(iclass)
        end if
        ! Search all the observations for items inside the range
        do iobservation=1,nobservations
            if (observations(iobservation).gt.lower .and. observations(iobservation).le.upper) then
                make_histogram(iclass) = make_histogram(iclass) + 1
            endif
        end do
    enddo

end function make_histogram

subroutine calculate_statistics(xdata,ndata,xmin,xmean,xmax,stddev,skewness,kurtosis)
    ! Calculate a list of statistics in xdata:
    !  ==> min, mean, max, standard deviation and coefficients of skewness and kurtosis
    ! Data assumed to have equal weights

    ! Inputs ==>
    ! Input data to calculate statistics on (size ndata)
    real(kind=8),intent(in) :: xdata(:)
    integer,intent(in) :: ndata
    ! Outputs ==>
    ! Min, mean, max, standard deviation and coefficients of skewness and kurtosis of data
    real(kind=8),intent(out) :: xmin, xmean, xmax, stddev, skewness, kurtosis
    ! Internal ==>
    real(kind=8) :: ndata_real

    ! Set real/floating-point version of ndata for use in calculations
    ndata_real = ndata

    ! Calculate min and max
    xmin = minval(xdata)
    xmax = maxval(xdata)

    ! Calculate mean of data
    xmean = sum(xdata) / ndata_real

    ! Calculate standard deviation
    stddev = sqrt(sum((xdata - xmean)**2)/ndata_real)

    ! Calculate coefficient of skewness
    ! Uses the Fisher definition
    skewness = sum((xdata - xmean)**3) / ((ndata_real) * stddev**3)

    ! Calculate coefficient of kurtosis
    kurtosis = sum(((xdata - xmean)/stddev)**4) / ndata_real - 3d0

end subroutine calculate_statistics

end module
