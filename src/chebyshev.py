'''
Implementation of numpy drop-in replacement for Chebyshev polynomial fit functions e02adf and e02aef
Wraps numpy implementation of Chebyshev fit
''' 

import numpy as np

# Climexp never uses more than one Chebyshev series at a time, so implement the series as a global variable
# Can be converted to an object if required
chebyshev_series = None

def chebyshev_fit(x_to_fit,y_to_fit,order,range_lower,range_upper):
    '''
    Set (or overwrite) computed global Chebyshev series
    '''
    global chebyshev_series
    chebyshev_series = np.polynomial.Chebyshev.fit(x_to_fit,y_to_fit,
                                                order,[range_lower,range_upper])
    
    
def chebyshev_evaluate(x_to_evaluate):
    '''
    Evaluate computed chebyshev series at x and return
    NOTE: the fit function MUST first be called or this will raise an error
    '''
    global chebyshev_series
    return chebyshev_series(x_to_evaluate)
        

def test_plot_chebyshev():
    '''
    General system test for the module
    Plots a test function with the Chebyshev polynomial fit
    '''
    import matplotlib.pyplot as plt

    # Set up test data
    rng = np.random.default_rng(seed=0)
    inrange = (-1.0,3.0)
    fitrange = inrange
    nfit = 100
    ntest = 1000
    testfunc = lambda x : (x-2) * x * (x+2)**2
    xfit = np.linspace(inrange[0],inrange[1],nfit)
    yfit = testfunc(xfit) + 5*rng.random(nfit) 

    # Fit Chebyshev polynomial
    chebyshev_fit(xfit,yfit,20,fitrange[0],fitrange[1])

    # Show plot of test data and fit curve
    xtest = np.linspace(inrange[0],inrange[1],ntest)
    plt.plot(xfit,yfit,'r.')
    plt.plot(xtest,chebyshev_evaluate(xtest))
    plt.show()

def test_value_chebyshev():
    '''
    Test a precise value
    '''
    # Set up test data
    testfunc = lambda x : (x-2) * x * (x+2)**2
    inrange = (0.0,1.0)
    fitrange = inrange
    xfit = np.linspace(inrange[0],inrange[1],40)
    yfit = testfunc(xfit)
    xtest = np.array([0.5])

    # Fit Chebyshev polynomial
    chebyshev_fit(xfit,yfit,20,fitrange[0],fitrange[1])
    result = chebyshev_evaluate(xtest)

    # Print and return test value
    print("Test result for Chebyshev polynomial:", result)


if __name__=="__main__":
    test_value_chebyshev()
    test_plot_chebyshev()

