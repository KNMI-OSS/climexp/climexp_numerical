! 2022/01/23 added line 1068-1072 to be adjust the value of the variable to a specific warming level

module points_to_print_at
    ! Define a set of points to plot at that can be redefined as needed
    ! By default, will be set to 10 values between 10 and 10000 inclusive, roughly uniform in log space
    ! This is a work-around for getting custom values in the trends in extremes table
    ! It is (for now) hard-coded to accept 10 values only to ensure compatibility with other parts of Climexp
    implicit none

    ! The number of return periods to calculate
    ! The first 10 are reserved for 10 to 10,000 years
    integer,parameter :: NUMRETURNPERIODS=11
    ! Put the precalculated return index in the last position
    integer,parameter :: PRECALCULATED_RETURN_INDEX=11
    ! Internal flag 
    logical :: ptpa_setup_defaults = .false.
    real, dimension(NUMRETURNPERIODS) :: ptpa_return_periods
    character(len=30), dimension(NUMRETURNPERIODS) :: ptpa_return_periods_string

    ! Make everything private except NUMRETURNPERIODS, the get and set functions
    private :: ptpa_setup_defaults, ptpa_return_periods, ptpa_return_periods_string
    private :: ptpa_internal_set_return_period, ptpa_setup

contains

    real function get_return_period(index)
        ! Get the return period at the requested index
        ! NOTE - to make sure index is within range
      integer,intent(in) :: index
      real :: return_period_checked
      
      call ptpa_setup
      return_period_checked = ptpa_return_periods(index)
      
      ! Check for valid return period and set to a small valid value if negative or zero
      !if (return_period_checked .le. 0.0) return_period_checked = 1e-4
      get_return_period = return_period_checked
      
    end function get_return_period

    character(len=30) function get_return_period_string(index)
        ! Get the return period at the requested index
        ! NOTE - to make sure index is within range
        integer,intent(in) :: index
        call ptpa_setup
        get_return_period_string = ptpa_return_periods_string(index)
    end function get_return_period_string

    subroutine set_return_period(return_period, index, is_integer_in)
        ! Set the return periods array at a specific index
        ! If is_integer is set, print the string version as an integer
        ! Otherwise, print as floating point
        ! Inputs
        real,intent(in)::return_period
        integer,intent(in)::index
        logical,intent(in),optional::is_integer_in
        ! Internal variables
        real::return_period_checked
        character(len=30)::to_string
        logical::is_integer = .false.
        ! Set optional argument
        if(present(is_integer_in)) is_integer = is_integer_in

        ! Set some defaults just in case to make sure values are filled
        ! If this has already been done, it won't overwrite anything
        call ptpa_setup

        ! Check for valid return period
        return_period_checked = return_period
        !if (return_period_checked .le. 0.0) return_period_checked = 1e-4

        ! Set the return periods internally
        call ptpa_internal_set_return_period(return_period_checked, index, is_integer)

    end subroutine set_return_period

    subroutine ptpa_internal_set_return_period(return_period, index, is_integer)
        ! Note: This version runs internally without an initial setup check
        !       Use set_return_period otherwise
        ! Set the return periods array at a specific index
        ! If is_integer is set, print the string version as an integer
        ! Otherwise, print as floating point

        ! Inputs
        real,intent(in)::return_period
        integer,intent(in)::index
        logical,intent(in)::is_integer
        ! Internal variables
        character(len=30)::to_string

        ! Set the value at the given index
        ptpa_return_periods(index) = return_period
        ! Set the string value, either formatting as an integer or as a float
        if (is_integer) then
            write(to_string,"(I6)") int(return_period)
        else
            write(to_string,"(f16.3)") return_period
        endif
        ptpa_return_periods_string(index) = trim(to_string)
        ! Debug the value set
        ! print '(a,i2,a,a)',"Setting return period at index",index,"to",trim(to_string)

    end subroutine ptpa_internal_set_return_period

    subroutine ptpa_setup
        ! Internal function for making sure some default values are setup
        integer :: i
        real :: x
        ! Check if defaults already set and return if so
        if (ptpa_setup_defaults) return
        ! Set up default return periods to use here
        ! These are the defaults previously used by Climexp
        do i=1,NUMRETURNPERIODS
            ! Calculates log return periods 1.0,1.3,1.6,2.0,...,3.6,4.0
            if ( mod(i,3) == 1 ) then
                x = 10.0**(1 + i/3)
            elseif ( mod(i,3) == 2 ) then
                x = 2. * 10.0**(1 + i/3)
            else
                x = 5. * 10.0**(i/3)
            endif
            ! Note: some parts of printpoint use this formulation, 
            !  however the covariant fitting seems to use the above
            ! Be careful and check whether the code can be safely unified
            ! x = 10**(1+(i-1)/3.)
            ! Set the return periods internally
            call ptpa_internal_set_return_period(x,i,.true.)
        enddo
        ptpa_setup_defaults = .true.

    end subroutine ptpa_setup

end module points_to_print_at


subroutine printpoint(i,f,ntype,x,s,yr)
    implicit none
    integer :: i,ntype,yr
    real :: f,x,s
    if ( ntype == 2 ) then
        if ( f > 0 ) then
            print '(i8,4g22.6,i12)',i,-log(-log(f)),x,s,1/(1-f),yr
        endif
    elseif ( ntype == 3 ) then
        print '(i8,4g22.6,i12)',i,-log(1-f),x,s,1/(1-f),yr
    elseif ( ntype == 4 ) then
        print '(i8,4g22.6,i12)',i,sqrt(-log(1-f)),x,s,1/(1-f),yr
    else
        write(0,*) 'histogram: error: unknown ntype ',ntype
        call exit(-1)
    endif
end subroutine printpoint
 

subroutine printreturnvalue(ntype,t,t25,t975,lweb)

!   print return times
    ! NOTE: Not using points_to_print_at to ensure compatibility with wider climexp_numerical
    ! Used by fit???.f90, stationlist
    use points_to_print_at

    implicit none
    integer :: ntype
    real :: x,t(10),t25(10),t975(10)
    integer :: xprint
    logical :: lweb
    integer :: i

    if ( ntype == 2 .or. ntype == 3 .or. ntype == 4 ) then ! extreme value  plot
        if ( lweb ) then
            do i=1,10,3
               x = 10**(1+i/3)
               xprint = x
               print '(a,i5,a,f16.3,a,f16.3,a,f16.3,a)' &
                    ,'# <tr><td>return value ', xprint &
                    ,' yr</td><td>',t(i),'</td><td>',t25(i) &
                    ,' ... ',t975(i),'</td></tr>'
               call print3untransf(t(i),t25(i),t975(i),0)
            enddo
        else
            do i=1,4
               x = 10**(i)
               xprint = x
               print '(a,i5,a,f16.3,a,2f16.3)' &
                    ,'# value for return period ',xprint,' year: ' &
                    ,t(i),' 95% CI ',t25(i),t975(i)
               call print3untransf(t(i),t25(i),t975(i),0)
            enddo
        endif
    endif
end subroutine printreturnvalue

function usinglogscale()
  ! Added wrapper function around getopts to prevent polluting local namespace
  implicit none
  include "getopts.inc" ! Used to access logscale option
  logical::usinglogscale

  usinglogscale = logscale
  
end function usinglogscale

function usingpercentages(assume)
    ! Function to determine whether we are using percentage units when calculating differences
    implicit none
    include "getopts.inc" ! Used to access logscale option
    character,intent(in) :: assume*(*)
    logical::usingpercentages

    ! 1. Using scale in absolute units (diff is t1/t2)
    ! 2. Using shift in log units (diff is log(t1) - log(t2) = log(t1/t2)
    ! For 2. we also convert out of log units for printing
    ! NOTE: We allow users to compute scale in log units but result may not be physically meaningul
    !       A warning is printed if this happens
    usingpercentages = .false.
    if (assume == 'scale') usingpercentages = .true.
    if ((assume == 'shift').and.(logscale)) then
        usingpercentages = .true.
    endif
    
end function usingpercentages

function using_absolute_quantities(assume)
    ! Function to determine whether we are using absolute quantities as opposed to log units in differences
    ! We have to be careful here because 
    implicit none
    include "getopts.inc" ! Used to access logscale option
    character,intent(in) :: assume*(*)
    logical :: output
    logical :: using_absolute_quantities

    ! 1. Using scale in absolute units (diff is t1/t2)
    ! 2. Using shift in log units (diff is log(t1) - log(t2) = log(t1/t2)
    ! For 2. we also convert out of log units for printing
    ! NOTE: We allow users to compute scale in log units but result may not be physically meaningul
    !       A warning is printed if this happens
    output = .true.
    if (logscale) then
        if (assume == 'shift') then
            ! Differences in this case are converted to absolute units
            output = .true.
        else
            ! Compute differences etc in log units
            ! This may not be physically meaningful and a warning is printed if this is done
            output = .false.
        endif
    endif
    using_absolute_quantities = output
    
  end function using_absolute_quantities

subroutine printcovreturnvalue(ntype,t,t25,t975,yr1a,cyr1a,yr2a,biasrt,lweb,plot,assume,lnone)

!   print return values for a set of fixed return times
  
    use points_to_print_at
    implicit none
    integer :: ntype,yr1a,yr2a
    real :: t(NUMRETURNPERIODS,4),t25(NUMRETURNPERIODS,4),t975(NUMRETURNPERIODS,4),biasrt
    real :: diff, diff25, diff975
    logical :: lweb,plot,lnone
    character assume*(*),cyr1a*(*)
    integer :: i, returnperiod, index
    logical lprintreturnvalue, usepercentages, uselogscale, useabsolute
    character units*30
    character valuechanging*30
    character returnperiodstr*40
    logical,external::usinglogscale
    logical,external::usingpercentages
    logical,external::using_absolute_quantities
    ! Hard-code some indices to print at, including 
    integer,parameter::nindices=5
    integer::indices(nindices) = (/1,4,7,10,11/)

    ! Debug printing values
    lprintreturnvalue = .false.

    ! Get whether we are using log scales from the form options
    uselogscale = usinglogscale()

    ! Are we using percentages?
    usepercentages = usingpercentages(assume)

    ! Are we printing differences as absolute units?
    useabsolute = using_absolute_quantities(assume)
    
    if ( ntype == 2 .or. ntype == 3 .or. ntype == 4 ) then ! extreme value  plot
        if ( usepercentages ) then
            units = ' %'
        else
            units = ' '
        end if
        if ( lweb ) then
            do index=1,nindices
                ! Get index to print at in the return period list
                i = indices(index)
                returnperiod = get_return_period(i)
                if ( lnone ) then
                    if ( lprintreturnvalue .or. biasrt == returnperiod ) then
                        print '(a,i5,a,f16.3,a,f16.3,a,f16.3,a)' &
                            ,'# <tr><td colspan=2>return value ',returnperiod &
                            ,' yr</td><td>',t(i,1),'</td><td>',t25(i,1) &
                            ,' ... ',t975(i,1),'</td></tr>'
                        call print3untransf(t(i,1),t25(i,1),t975(i,1),-1)
                    end if
                else
                    if ( lprintreturnvalue .or. biasrt == returnperiod ) then
                        print '(a,i5,3a,f16.3,a,f16.3,a,f16.3,a)' &
                            ,'# <tr><td>return value ',returnperiod &
                            ,' yr</td><td>',cyr1a,'</td><td>' &
                            ,t(i,1),'</td><td>',t25(i,1) &
                            ,' ... ',t975(i,1),'</td></tr>'
                        print '(a,i4,a,f16.3,a,f16.3,a,f16.3,a)' &
                            ,'# <tr><td>&nbsp;</td><td>',yr2a &
                            ,'</td><td>',t(i,2),'</td><td>',t25(i,2) &
                            ,' ... ',t975(i,2),'</td></tr>'
                    end if
                    if ( i == 10 .or. assume == 'both' ) then
                        valuechanging = 'intensity'
                        if (uselogscale) then
                           if (useabsolute) then
                              valuechanging = "intensity (original units)"
                           else
                              valuechanging = "log<sub>10</sub>(intensity)"
                           endif
                        end if
                        returnperiodstr = ''
                        ! If writing 'both', specify which return period as every value is different
                        if (assume == 'both') then
                            returnperiodstr = get_return_period_string(i)
                            returnperiodstr = ' for period '//trim(returnperiodstr)//' yr'
                        endif

                        ! Print the differences
                        print '(3a,i4,3a,f16.3,a,f16.3,a,f16.3,a)' &
                            ,'# <tr><td>change in '//trim(valuechanging)//' ',cyr1a,'-',yr2a, &
                            trim(returnperiodstr)//'</td><td>diff',trim(units), &
                            '</td><td>',t(i,3),'</td><td>',t25(i,3) &
                            ,' ... ',t975(i,3),'</td></tr>'
                    end if
                    if ( lprintreturnvalue ) then
                        call print3untransf(t(i,1),t25(i,1),t975(i,1),yr1a)
                        call print3untransf(t(i,2),t25(i,2),t975(i,2),yr2a)
                    end if
                end if
            enddo
            if ( plot ) then ! output for stationlist
                ! Don't need to show the extra return value at t(2) in the plot, limit to 10
                do i=1,10
                    returnperiod = get_return_period(i)
                    if ( lnone ) then
                        write(11,'(4g20.4,i6,a)') t(i,1),t25(i,1), &
                            t975(i,1),returnperiod,0,' return value'
                    else
                        write(11,'(4g20.4,2a)') t(i,1),t25(i,1), &
                            t975(i,1),returnperiod,cyr1a,' return value'
                        write(11,'(4g20.4,i5,a)') t(i,2),t25(i,2), &
                            t975(i,2),10**(1+i/3.),yr2a,' return value'
                    end if
                end do
            end if
        else
            ! Print all the values for debugging
            do i=1,NUMRETURNPERIODS
                returnperiod = get_return_period(i)
                returnperiodstr = get_return_period_string(i)
                if ( lnone ) then
                    print '(a,a,a,f16.3,a,2f16.3)' &
                        ,'# value for return period ',trim(adjustl(returnperiodstr)) &
                        ,': ' &
                        ,t(i,1),' 95% CI ',t25(i,1),t975(i,1)
                    call print3untransf(t(i,1),t25(i,1),t975(i,1),0)
                else
                    print '(a,3a,f16.3,a,2f16.3)' &
                        ,'# value for return period '//trim(adjustl(returnperiodstr)) &
                        ,' year at yr=',cyr1a,': ' &
                        ,t(i,1),' 95% CI ',t25(i,1),t975(i,1)
                    print '(a,a,i4,a,f16.3,a,2f16.3)' &
                        ,'# value for return period '//trim(adjustl(returnperiodstr)) &
                        ,' year at yr=',yr2a,': ' &
                        ,t(i,2),' 95% CI ',t25(i,2),t975(i,2)
                    print '(a,3a,f16.3,a,2f16.3)' &
                        ,'# value for return period '//trim(adjustl(returnperiodstr)) &
                        ,' year, difference',trim(units),': ' &
                        ,t(i,3),' 95% CI ',t25(i,3),t975(i,3)
                    call print3untransf(t(i,1),t25(i,1),t975(i,1),yr1a)
                    call print3untransf(t(i,2),t25(i,2),t975(i,2),yr2a)
                end if
            enddo
        endif
    endif
end subroutine printcovreturnvalue


subroutine printreturntime(year,xyear,tx,tx25,tx975,lweb)

!   print return time of year

    implicit none
    integer :: year
    real :: xyear,tx,tx25,tx975
    logical :: lweb
            
    if ( xyear < 1e33 ) then
        if ( lweb ) then
            if ( year == 9999 ) then
                print '(a,g16.5,a,g16.5,a,g16.5,a,g16.5,a)' &
                    ,'# <tr><td>return period ',xyear,'</td><td>' &
                    ,tx,'</td><td>',tx25,' ... ',tx975,'</td></tr>'
                print '(a,g16.5,a,g16.5,a,g16.5,a,g16.5,a)' &
                    ,'# <tr><td>probability ',xyear,'</td><td>' &
                    ,1/tx,'</td><td>',1/tx975,' ... ',1/tx25,'</td></tr>'
            else
                print '(a,g16.5,a,i5,a,g16.5,a,g16.5,a,g16.5,a)' &
                    ,'# <tr><td>return period ',xyear,'(',year &
                    ,')</td><td>' &
                    ,tx,'</td><td>',tx25,' ... ',tx975,'</td></tr>'
                print '(a,g16.5,a,i5,a,g16.5,a,g16.5,a,g16.5,a)' &
                    ,'# <tr><td>probability ',xyear,'(',year &
                    ,')</td><td>' &
                    ,1/tx,'</td><td>',1/tx975,' ... ',1/tx25,'</td></tr>'
            end if
        else
            print '(a,f16.5,a,i4,a,f16.5,a,2f18.5)','# return time ' &
                ,xyear,' (',year,') = ',tx,' 95% CI ',tx25,tx975
        endif
    endif
end subroutine printreturntime


subroutine printcovreturntime(year,xyear,idmax,tx,tx25,tx975,cyr1a,yr2a,cyr2b,lweb,plot,assume,lnone,i12)

!   print return time of year at cov1 and cov2 (and optionally cov3)

    implicit none
    integer :: year,yr2a,i12
    real :: xyear,tx(4),tx25(4),tx975(4)
    logical :: lweb,plot,lnone
    character cyr1a*(*),cyr2b*(*),idmax*(*),assume*(*)
    integer :: i,nj
    character atx(4)*16,atx25(4)*16,atx975(4)*16, &
        ainvtx(4)*16,ainvtx25(4)*16,ainvtx975(4)*16
    character return_value_str*100
    logical,external :: usinglogscale

    if ( xyear < 1e33 ) then
        if ( cyr2b /= '0000' .and. cyr2b(1:1) /= '-' ) then
            nj = 4
        else
            nj = 3
        end if
        do i=1,nj
            call val_or_inf(atx(i),tx(i),lweb)
            call val_or_inf(atx25(i),tx25(i),lweb)
            call val_or_inf(atx975(i),tx975(i),lweb)
            call val_or_inf(ainvtx(i),1/tx(i),lweb)
            if ( tx25(i) < 1e-20 ) tx25(i) = 1e-20
            if ( tx975(i) < 1e-20 ) tx975(i) = 1e-20
            call val_or_inf(ainvtx25(i),1/tx25(i),lweb)
            call val_or_inf(ainvtx975(i),1/tx975(i),lweb)
        end do
        ! String to print for the return value
        ! Prints extra if using log units
        if (usinglogscale()) then
            write(return_value_str,'(g16.5,a,g16.5,a,g16.5)') 10.0**xyear, &
                & ' in original units; log<sub>10</sub>(',10.0**xyear,') = ',xyear
        else
            write(return_value_str,'(g16.5)') xyear
        endif
        if ( lweb ) then
            if ( .not. lnone ) then
                if ( i12 == 1 ) then
                    ! Return period 1 (past comparison)
                    if ( idmax == ' ' ) then
                        print '(a,i5,a,a,9a)' &
                            ,'# <tr><td><!--atr2-->return period event ',year, &
                            ' (value ',return_value_str,')</td><td>',cyr1a,'</td><td>',atx(1), &
                            '</td><td>',atx25(1),' ... ',atx975(1),'</td></tr>'
                    else
                        print '(a,i5,a,a,11a)' &
                            ,'# <tr><td><!--atr2-->return period event ',year, &
                            ' (value ',return_value_str,' at ',trim(idmax),')</td><td>',cyr1a,'</td><td>',atx(1), &
                            '</td><td>',atx25(1),' ... ',atx975(1),'</td></tr>'
                    end if
                    print '(9a)' &
                        ,'# <tr><td>probability</td><td>',cyr1a,'</td><td>',ainvtx(1), &
                        '</td><td>',ainvtx975(1),' ... ',ainvtx25(1),'</td></tr>'
                    ! Return period 2 (present)
                    if ( idmax == ' ' ) then
                        print '(a,i5,a,a,a,i5,7a)' &
                            ,'# <tr><td><!--atr1-->return period event ',year, &
                            ' (value ',return_value_str,')</td><td>',yr2a,'</td><td>',atx(2), &
                            '</td><td>',atx25(2),' ... ',atx975(2),'</td></tr>'
                    else
                        print '(a,i5,a,a,3a,i5,7a)' &
                            ,'# <tr><td><!--atr1-->return period ',year, &
                            ' (value ',return_value_str,' at ',trim(idmax),')</td><td>',yr2a,'</td><td>',atx(2), &
                            '</td><td>',atx25(2),' ... ',atx975(2),'</td></tr>'
                    end if
                    print '(a,i5,7a)' &
                        ,'# <tr><td>probability</td><td>',yr2a,'</td><td>',ainvtx(2), &
                        '</td><td>',ainvtx975(2),' ... ',ainvtx25(2),'</td></tr>'
                    ! Ratio of probabilities
                    print '(8a)' &
                            ,'# <tr><td><!--atra-->probability ratio</td><td>&nbsp;', &
                            '</td><td>',atx(3),'</td><td>',atx25(3), &
                            ' ... ',atx975(3),'</td></tr>'
                    if ( tx(3) < 1 ) then
                        print '(8a)' &
                            ,'# <tr><td>inverse probability ratio</td><td>&nbsp;', &
                            '</td><td>',ainvtx(3),'</td><td>',ainvtx975(3), &
                            ' ... ',ainvtx25(3),'</td></tr>'
                    end if
                else
                    if ( nj > 3 ) then
                        if ( idmax == ' ' ) then
                            print '(a,i5,a,a,9a)' &
                                ,'# <tr><td>return period event ',year, &
                                ' (value ',return_value_str,')</td><td>',cyr2b,'</td><td>',atx(4), &
                                '</td><td>',atx25(4),' ... ',atx975(4),'</td></tr>'
                        else
                            print '(a,i5,a,a,11a)' &
                                ,'# <tr><td>return period event ',year, &
                                ' (value ',return_value_str,' at ',trim(idmax),')</td><td>',cyr2b,'</td><td>',atx(4), &
                                '</td><td>',atx25(4),' ... ',atx975(4),'</td></tr>'
                        end if
                        print '(9a)' &
                            ,'# <tr><td>probability</td><td>',cyr2b,'</td><td>',ainvtx(4), &
                            '</td><td>',ainvtx975(4),' ... ',ainvtx25(4),'</td></tr>'
                    end if
                end if
            else
                print '(a,a,7a)' &
                    ,'# <tr><td colspan=2><!--atr2-->return period ',return_value_str, &
                    '</td><td>',atx(1), &
                    '</td><td>',atx25(1),' ... ',atx975(1),'</td></tr>'
                print '(7a)' &
                    ,'# <tr><td colspan=2><!--atr2-->probability </td><td>',ainvtx(1), &
                    '</td><td>',ainvtx975(1),' ... ',ainvtx25(1),'</td></tr>'
            end if
        else
            if ( .not. lnone ) then
                print '(a,i4,7a)' &
                    ,'# return time ',year,' at yr=',cyr1a &
                    ,' = ',atx(1),' 95% CI ',atx25(1),atx975(1)
                print '(a,i4,a,i4,5a)' &
                    ,'# return time ',year,' at yr=',yr2a &
                    ,' = ',atx(2),' 95% CI ',atx25(2),atx975(2)
                print '(a,i4,5a)','# return time ',year,' ratio = ' &
                    ,atx(3),' 95% CI ',atx25(3),atx975(3)
                if ( nj > 3 ) then
                    print '(a,i4,7a)' &
                        ,'# return time ',year,' at yr=',cyr2b &
                        ,' = ',atx(4),' 95% CI ',atx25(4),atx975(4)
                end if
            else
                print '(a,a,7a)','# return time ',return_value_str,' = ',atx(1),' 95% CI ',atx25(1),atx975(1)                
            end if
        end if
        if ( plot ) then    ! output for stationlist
            write(11,'(3g20.4,2a)') tx(1),tx25(1),tx975(1),cyr1a,' return time'
            write(11,'(3g20.4,i6,a)') tx(2),tx25(2),tx975(2),yr2a,' return time'
            write(11,'(3g20.4,a)') tx(3),tx25(3),tx975(3),' ratio'
        end if
    endif
end subroutine printcovreturntime


subroutine printcovpvalue(txtx,nmc,nens,lweb,plot)

!   print out the p-value at which the ratio of return time is unequal to 1 (FAR unequal to 0)

    implicit none
    integer :: nmc,nens
    real :: txtx(nmc,3)
    logical :: lweb,plot
    integer :: i
    real :: p,one
    one = 1
    call invgetcut(p,one,nens,txtx(1,3))
    if ( p > 0.5 ) p = 1-p
    if ( lweb ) then
        print '(2a,f7.4,2a)','# <tr><td><i>p</i>-value probability ratio (one-sided)', &
            '</td><td>&#8800; 1</td><td>',p,'</td><td>&nbsp;</td>', &
            '</tr>'
    else
        print '(a,f7.4,a)','# p-value for ratio/=1 (one-sided) ',p
    end if
    if ( plot ) then    ! output for stationlist
        write(11,'(g20.4,a)') p,' p-value (one-sided)'
    end if
end subroutine printcovpvalue


subroutine plotreturnvalue(ntype,t25,t975,n)

!   print return value at a few times

    use points_to_print_at
    implicit none
    integer :: n
    real :: t25(10),t975(10)
    logical :: lweb
    integer :: i,ntype,init
    save init
    real :: x,f
    character fx*10,ff*10
    data init /0/

    if ( ntype == 2 .or. ntype == 3 .or. ntype == 4 ) then ! extreme value  plot
        if ( init == 0 ) then
            init = 1
            if ( ntype == 2 ) then
                fx = 'Gumbel(T)'
            else if ( ntype == 3 ) then
                fx = 'log(T)'
            else if ( ntype == 4 ) then
                fx = 'sqrtlog(T)'
            else
                fx = '???'
            end if
            ff = 'fit'      ! maybe later propagate nfit and make beautiful...
            print '(5a)','#     n            ',fx, &
                '            Y                     ',ff, &
                '            T              date'
        end if
        ! Note: only looping over 1 to 10, not any extra values in points_to_print_at
        do i=1,10
            x = log10(get_return_period(i))
            x = x + log10(real(n))
            f = 1 - 10.**(-x)
            call printpoint(0,f,ntype,-999.9,t25(i),0)
        enddo
        print '(a)'
        do i=1,10
            x = log10(get_return_period(i))
            x = x + log10(real(n))
            f = 1 - 10.**(-x)
            call printpoint(0,f,ntype,-999.9,t975(i),0)
        enddo
        print '(a)'
    endif
end subroutine plotreturnvalue


subroutine val_or_inf(atx,tx,lweb)
    implicit none
    real :: tx
    character atx*(*)
    logical :: lweb
    if ( abs(tx) < 1e19 .and. abs(tx) > 1e-19 ) then
        write(atx,'(g16.5)') tx
    else
        if ( abs(tx) >= 1e33 .or. abs(tx) < 1e-33 ) then
            atx = 'undefined'
        else if ( abs(tx) >= 1e19 ) then
            if ( lweb ) then
                atx = '&infin;'
            else
                atx = 'infinity'
            end if
        else
            atx = '0'
        end if
        if ( tx < 0 ) then
            atx = '-'//trim(atx)
        end if
    end if
end subroutine val_or_inf


subroutine plot_ordered_points(xx,xs,yrs,ntot,ntype,nfit, &
    frac,a,b,xi,j1,j2,nblockyr,nblockens,minindx,mindata,pmindata, &
    yr2a,xyear,snorm,lchangesign,lwrite,last)

!   Gumbel or (sqrt) logarithmic plot

    implicit none
    integer,intent(in) :: ntot,ntype,nfit,j1,j2,nblockyr,nblockens,yr2a
    integer,intent(in) :: yrs(5,0:ntot)
    real,intent(in) :: xx(ntot),frac,a,b,xi
    real,intent(inout) :: xs(ntot)
    real,intent(in) :: minindx,mindata,pmindata,xyear,snorm
    logical,intent(in) :: lchangesign,lwrite,last
    integer :: i,j,m,it,nzero
    real :: f,ff,s,x,z,sqrt2,tmax
    real,save :: smin=3e33,smax=-3e33
    character string*100
    logical :: lprinted
    real :: erf
    real,external :: gammp,gammq,invcumgamm,invcumpois,serfi
    real :: scalingpower
    common /c_scalingpower/ scalingpower

    if ( lwrite ) then
        print *,'plot_ordered_points: xyear = ',xyear
        print *,'                    a,b,xi = ',a,b,xi
        print *,'  minindx,mindata,pmindata = ',minindx,mindata,pmindata
    end if
    call keepalive1('Output',0,ntot+100)
    call nrsort(ntot,xx)
    do nzero = 1,ntot
        if ( xx(nzero) /= 0 ) exit
    end do
    nzero = nzero - 1
    do i=1,ntot+100
        if ( mod(i,1000) == 0 ) call keepalive1('Output',i,ntot+100)
    
    !   search unsorted year that belongs to this point
    !   attention: quadratic algorithm!
    
        if ( i <= ntot ) then
            if ( xx(i) < 1e33 ) then
                do j=1,ntot
                    if ( xs(j) == xx(i) ) goto 790
                end do
                print *,'warning: cannot find year for x = ',x
            end if
        end if
        j = 0 ! yrs(:,0) = 0 is printed
790     continue
        if ( j > 0 ) xs(j) = 3e33

        if ( i <= ntot ) then
            f = real(i)/real(ntot+1)
            if ( xx(i) < 1e33 ) then
                x = xx(i)
            else
                x = -999.9
            end if
        else
            f = 1 - 1/real(ntot+1)*0.9**(i-ntot)
            if ( abs(f-1) < 1e-6 ) goto 800
            x = -999.9
        endif
        ff = f
        if ( ( nfit == 4 .or. nfit == 5 ) .and. ( nblockyr > 1 .or. nblockens > 1 ) ) then
            f = 1 - (1-f)/(nblockyr*nblockens)
        end if
        if ( frac /= 1 ) f = 1-(1-f)*frac
        if ( nfit == 0 ) then
            s = -999.9
        elseif ( nfit == 1 ) then
        !   Poisson distribution - only the last point of a bin makes sense
            if ( i > ntot .or. xx(min(ntot,i)) /= xx(min(ntot,i+1)) ) then
                f = snorm*f
                if ( minindx > 0 ) then
                    f = f + gammq(minindx+0.5,a)
                endif
                s = invcumpois(f,1-f,a)
            else
                s = -999.9
            endif
        elseif ( nfit == 2 ) then
        !   Gaussian distribution
            sqrt2 = sqrt(2.)
            ff = 2*snorm*f
            if ( minindx > -1e33 ) then
                ff = ff + erf((minindx-a)/(sqrt2*b))
            else
                ff = ff - 1
            endif
        !   netlib routine
            z = serfi(ff)
            s = a + sqrt2*b*z
            if ( lchangesign ) s = -s
        elseif ( nfit == 3 ) then
        !   Gamma distribution plus possible delta at zero
            if ( i <= nzero ) then
                s = 0
            else
                ff = (f*ntot - nzero)/(ntot - nzero)
                ff = snorm*ff
                if ( minindx > 0 ) then
                    ff = ff + gammp(a,minindx/b)
                endif
                if ( lchangesign ) ff = 1 - ff
                s = invcumgamm(ff,1-ff,a,abs(b))
            end if
        elseif ( nfit == 4 ) then
        !   Gumbel distribution
            s = snorm*ff
            if ( minindx > -1e33 ) then
                s = s + exp(-exp(-(minindx-a)/b))
            endif
            s = a - b*log(-log(s))
            if ( lchangesign ) s = -s
        elseif ( nfit == 5 ) then
        !   GEV distribution
            s = snorm*ff
            if ( minindx > -1e33 ) then
                s = s + exp(-(1+(minindx-a)/b)**(-1/xi))
            endif
            if ( xi == 0 ) then
                s = a - b*log(-log(s))
            else
                s = a + b/xi*((-log(s))**(-xi)-1)
            endif
            if ( lchangesign ) s = -s
        elseif ( nfit == 6 ) then
        !   GPD distribution - only in the tail
            if ( f < pmindata/100 ) then
                s = -999.9
            else
                s = (f - pmindata/100)/(1 - pmindata/100)
                if ( abs(xi) < 1e-4 ) then
                    s = b*(-log(1-s) + 0.5*xi*(log(1-s))**2)
                else
                    s = b/xi*((1-s)**(-xi) - 1)
                endif
                s = mindata + s
                if ( lchangesign ) s = -s
            endif
        else
            write(0,*) 'histogram: error: unknown distribution ',nfit
            call exit(-1)
        endif
    
        if ( lchangesign ) then
            if ( x /= -999.9 .and. x < 1e33 ) x = -x
        endif
        call printpoint(i,f,ntype,x,s,10000*yrs(2,j)+100*yrs(3,j)+yrs(4,j))
        smin = min(s,smin)
        smax = max(s,smax)
        if ( i > ntot .and. (1-f)*(j2-j1+1) < 0.0001 ) goto 800
    enddo
800 continue
    if ( last ) then
        print '(a)'
        print '(a)'
        if ( xyear /= 3e33 ) then
            call printpoint(0,1/real(ntot+1),ntype,-999.9,xyear,10000*yr2a)
            call printpoint(0,f,ntype,-999.9,xyear,10000*yr2a)
        else
            call printpoint(0,1/real(ntot+1),ntype,-999.9,-999.9,10000*yr2a)
            call printpoint(0,f,ntype,-999.9,-999.9,10000*yr2a)
        endif
        call print_xtics(6,ntype,ntot,j1,j2)
        if ( scalingpower /= 1 ) then
            call printy2tics(6,smin,smax,scalingpower)
        end if
    end if
end subroutine plot_ordered_points


subroutine print_bootstrap_message(ndecor,j1,j2)
    implicit none
    integer :: ndecor,j1,j2
    if ( ndecor == 1 ) then
        print '(3a)' &
            ,'# The error margins were computed with a ' &
            ,'bootstrap method that assumes all points are ' &
            ,'temporally independent.'
    elseif ( j1 /= j2 ) then
        print '(2a,i4,a)' &
            ,'# The error margins were computed with a ' &
            ,'moving block bootstrap with block size ' &
            ,ndecor,' months.'
    else
        print '(2a,i4,a)' &
            ,'# The error margins were computed with a ' &
            ,'moving block bootstrap with block size ' &
            ,ndecor,' years.'
    endif
end subroutine print_bootstrap_message


subroutine print_xtics(unit,ntype,ntot,j1,j2)

!   convert Gumbel/log variates to return periods

    implicit none
    integer :: unit,ntype,ntot,j1,j2
    integer :: tmax,it,i
    real :: f,t,xmax
    character string*80
    logical :: lprinted
    ! This is designed to fix a weirdness with Fortran printing single backslashes
    character (len=1), parameter :: backslash = achar (92)

    tmax = max(10000,int((ntot+1)/real(j2-j1+1))) ! in years
    if ( ntype == 2 ) then
        write(unit,'(a,f8.4,a)') '#@ set xrange [:', &
            -log(-log(1-1/(tmax*real(j2-j1+1)+1))),']'
    elseif ( ntype == -2 ) then
        t = (tmax*real(j2-j1+1)+1)/10 ! 10 times smaller range as we go two sides
        xmax = log(t)
        write(unit,'(a,f8.4,a,f8.4,a)') '#@ set xrange [',-xmax,':',xmax,']'
    elseif ( ntype == 3 ) then
        write(unit,'(a,f8.4,a)') '#@ set xrange [:', &
            -log(1/(tmax*real(j2-j1+1)+1)),']'
    elseif ( ntype == 4 ) then
        write(unit,'(a,f8.4,a)') '#@ set xrange [:', &
            sqrt(-log(1/(tmax*real(j2-j1+1)+1))),']'
    else
        write(0,*) 'histogram: error: unknown ntype ',ntype
        call exit(-1)
    endif
    write(unit,'(a)') '#@ set xtics ('//backslash
    it = 1
    do i=1,100
        f = 1-1/(it*real(j2-j1+1))
        t = it*real(j2-j1+1)
        write(string,'(a,i1,a)') '(a,i',1+int(log10(real(it))),',a,f8.4,a)'
        lprinted = .TRUE. 
        if ( ntype == 2 ) then
            if ( f > 0 ) then
                if ( it > 10 .and. mod(i,3) /= 1 ) then
                    write(unit,'(a,f8.4,a)') '#@ "" ',-log(-log(f)),backslash
                else
                    write(unit,string) '#@ "',it,'" ',-log(-log(f)),backslash
                endif
            else
                lprinted = .FALSE. 
            endif
        elseif ( ntype == -2 ) then
            if ( mod(i,3) /= 1 ) then
                write(unit,'(a,f8.4,a)') '#@ "" ',log(t),','//backslash
                write(unit,'(a,f8.4,a)') '#@ "" ',log(t),backslash
            else
                write(unit,string) '#@ "',it,'" ',log(t),','//backslash
                write(unit,string) '#@ "1/',-it,'" ',log(t),backslash
            endif
        elseif ( ntype == 3 ) then
            if ( it > 10 .and. mod(i,3) /= 1 ) then
                write(unit,'(a,f8.4,a)') '#@ "" ',-log(1-f),backslash
            else
                write(unit,string) '#@ "',it,'" ',-log(1-f),backslash
            endif
        elseif ( ntype == 4 ) then
            if ( it > 10 .and. mod(i,3) /= 1 ) then
                write(unit,'(a,f8.4,a)') '#@ "" ',sqrt(-log(1-f)),backslash
            else
                write(unit,string) '#@ "',it,'" ',sqrt(-log(1-f)),backslash
            endif
        else
            write(0,*) 'histogram: error: unknown ntype ',ntype
            call exit(-1)
        endif
        if ( mod(i,3) == 2 ) then
            it = nint(2.5*it)
        else
            it = 2*it
        endif
        if ( it > tmax ) goto 801
        if ( lprinted ) write(unit,'(a)') '#@ ,'//backslash
    enddo
801 continue
    write(unit,'(a)') '#@ )'
end subroutine print_xtics


subroutine printy2tics(unit,smin,smax,scalingpower)

!   print the gnuplot command to replace the RH Y-axis with the original units

    implicit none
    integer :: unit
    real :: smin,smax,scalingpower
    integer :: i,j,n,pow,i1,i2
    real :: delta,s
    character string*100
    logical :: wroteline
    ! This is designed to fix a weirdness with Fortran printing single backslashes
    character (len=1), parameter :: backslash = achar (92)

    if ( scalingpower == 1 ) return
    if ( smin < 0 ) smin = 0
    if ( scalingpower == 0 ) then
        smin = 10.**smin
        smax = 10.**smax
    else
        smin = smin**(1/scalingpower)
        smax = smax**(1/scalingpower)
    end if
    delta = (smax-smin)/5
!   round to 1,2,5*10^pow
    pow = int(log10(delta))
    delta = delta/10**pow
    if ( delta < 2 ) then
        delta = 1
    else if ( delta < 5 ) then
        delta = 2
    else
        delta = 5
    end if
    delta = delta*10**pow
    i1 = int(smin/delta)
    i2 = int(smax/delta) + 1

    write(unit,'(a)') '#@ set ytics nomirror'
    write(unit,'(a)') '#@ set y2tics ('//backslash
    do i = i1,i2
        !  Flag whether a line was written, otherwise the comma at the end will cause an error
        wroteline = .false.
        if ( nint(i*delta) >= 1 .or. i == 0 ) then
            if ( i == 0 ) then
                string = '(a,i1,a,f15.4,a)'
            else
                n = 1+int(0.01+log10(i*delta))
                n = max(1,min(9,n))
                write(string,'(a,i1,a)') '(a,i',n,',a,f15.4,a)'
            end if
            if ( scalingpower == 0 ) then
                if ( i /= 0 ) then
                    write(unit,string) '#@ "',nint(i*delta), &
                         '" ',log10(i*delta),backslash
                    wroteline = .true.
                end if
            else
                write(unit,string) '#@ "',nint(i*delta), &
                     '" ',(i*delta)**(scalingpower),backslash
                wroteline = .true.
            end if
        else
            n = int(-0.01-log10(i*delta))
            n = max(1,min(n,7))
            write(string,'(a,i1,a,i1,a)') '(a,f',n+2,'.',n, &
                ',a,f15.4,a)'
            if ( scalingpower == 0 ) then
                write(unit,string) '#@ "',i*delta, &
                     '" ',log10(i*delta),backslash
                wroteline = .true.
            else
                write(unit,string) '#@ "',i*delta, &
                     '" ',(i*delta)**(scalingpower),backslash
                wroteline = .true.
            end if
        end if
        if (( i < i2 ).and.(wroteline)) then
            write(unit,'(a)') '#@ ,'//backslash
        end if
    end do
    write(unit,'(a)') '#@ )'
end subroutine printy2tics


subroutine plot_tx_cdfs(txtx,nmc,nens,ntype,j1,j2)

!   make a file to plot CDFs

    integer :: nmc,nens,ntype,j1,j2
    real :: txtx(nmc,4)
    integer :: iens,j
    real :: logtxtx(4)
    write(10,'(a)') '# fraction, return values in the past '// &
        'climate, current climate and difference (sorted), '
    if ( ntype == 2 ) then
        write(10,'(a)') '# repeated as gumbel transforms'
    else if ( ntype == 3 ) then
        write(10,'(a)') '# repeated as logarithmic transforms'
    else if ( ntype == 4 ) then
        write(10,'(a)') '# repeated as sqrt-logarithmic transforms'
    else
        write(0,*) 'plot_tx_cdfs: error: unknown value for ntype ',ntype
        call exit(-1)
    end if
    if ( nens > nmc ) then
        write(0,*) 'plot_tx_cdfs: internal error: nens>nmc ',nens,nmc
        nens = nmc
    end if
    do iens=1,nens
        do j=1,4
            if ( j == 3 ) cycle
            if ( ntype == 2 ) then
                if ( txtx(iens,j) < 1e20 .and. txtx(iens,j) > 1 ) then
                    logtxtx(j) = -log(1-1/((j2-j1+1)*txtx(iens,j)))
                    if ( logtxtx(j) > 0 ) then
                        logtxtx(j) = -log(logtxtx(j))
                    else
                        logtxtx(j) = 3e33
                    end if
                else
                    logtxtx(j) = 1e20
                end if
            else if ( ntype == 3 ) then
                if ( txtx(iens,j) < 1e20 .and. txtx(iens,j) > 0 ) then
                    logtxtx(j) = log(txtx(iens,j)*(j2-j1+1))
                else
                    logtxtx(j) = 1e20
                end if
            else if ( ntype == 4 ) then
                if ( txtx(iens,j) < 1e20 .and. txtx(iens,j) > 1 ) then
                    logtxtx(j) = sqrt(log(txtx(iens,j)*(j2-j1+1)))
                else
                    logtxtx(j) = 1e20
                end if
            end if
        end do
        !   simple logscale for the time being
        if ( txtx(iens,3) < 1e20 .and. txtx(iens,3) > 0 ) then
            logtxtx(3) = log(txtx(iens,3)*(j2-j1+1))
        else
            logtxtx(3) = 1e20
        end if
        write(10,'(f6.4,8g16.5)') iens/real(nens+1), &
            (txtx(iens,j),j=1,3),(logtxtx(j),j=1,3), &
            (txtx(iens,j),j=4,4),(logtxtx(j),j=4,4)
    end do
end subroutine plot_tx_cdfs

subroutine getreturnlevelsforperiod(a,b,xi,alpha,beta,cov1,cov2,cov3,covreturnlevel,j1,j2,assume,returnperiod,returnlevels)
    ! Get the return levels for a return period "returnperiod" in years
    ! Uses the external function covreturn level, which uses a specified distribution to calculate the return values
    ! Return value is a 4-vector with the structure (cov1 return period, cov2 return period, difference, cov3 return period)
    implicit none
    include "getopts.inc" ! Used to access logscale option
    ! Inputs
    integer,intent(in) :: j1,j2
    real,intent(in) :: a,b,xi,alpha,beta,cov1,cov2,cov3,returnperiod
    character,intent(in) :: assume*(*)
    real,external :: covreturnlevel
    logical,external :: usinglogscale, usingpercentages
    ! Internal variables
    real :: tempperiod
    ! Outputs
    real, dimension(4),intent(out) :: returnlevels

    ! Calculate the return level for cov1
    tempperiod = returnperiod ! Copy value to prevent functions modifying this version of x
    returnlevels(1) = covreturnlevel(a,b,xi,alpha,beta,tempperiod,cov1)
    ! Calculate the return level for cov2
    tempperiod = returnperiod
    returnlevels(2) = covreturnlevel(a,b,xi,alpha,beta,tempperiod,cov2)
    ! Calculate return level for cov3 if provided
    if ( cov3 < 1e33 ) then
        tempperiod = returnperiod
        returnlevels(4) = covreturnlevel(a,b,xi,alpha,beta,tempperiod,cov3)
    else
        returnlevels(4) = 3e33
    end if
    ! Calculate the difference of cov2 and cov1 return levels
    returnlevels(3) = returnlevels(2) - returnlevels(1)

    ! If using log units with shift, convert back to absolute units
    ! This is because diff = log(t2) - log(t1) = log(t2/t1)
    ! So 10**diff - 1 = (t2 - t1) / t1
    ! A warning should be printed if using 'scale' and log because this is undefined behaviour
    if ((assume == 'shift').and.(usinglogscale())) then
        returnlevels(3) = 10.0**returnlevels(3) - 1.0
    endif

    ! If using scale, then calculate (t2 - t1) / t1 directly
    ! Note that log units here are not converted to absolute units
    ! This may not be physically meaningful and a warning is printed if this happens
    if ( assume == 'scale' ) then ! relative change in % relative to *past*
        returnlevels(3) = returnlevels(3)/returnlevels(1)
    end if

    ! If using percentages, convert the difference to a percentage
    if (usingpercentages(assume)) then
        returnlevels(3) = 100.0 * returnlevels(3)
    endif
    ! The 2.5% and 97.5% levels will be calculated using these values in fit[func]cov
    ! where func is one of gau, gev, gpd or gum

end subroutine getreturnlevelsforperiod


subroutine getreturnlevels(a,b,xi,alpha,beta,cov1,cov2,cov3,covreturnlevel,j1,j2,assume,t)
    ! Get the return levels for a set of return periods from 10 to 10,000 years
    ! Uses the external function covreturn level, which uses a specified distribution to calculate the return values
    use points_to_print_at
    implicit none
    include "getopts.inc" ! Used to access logscale option
    integer,intent(in) :: j1,j2
    real,intent(in) :: a,b,xi,alpha,beta,cov1,cov2,cov3
    real,intent(out) :: t(NUMRETURNPERIODS,4)
    character,intent(in) :: assume*(*)
    character :: newline*4
    real,external :: covreturnlevel
    integer :: i
    real :: x,xx,tdiff,return_period
    real, dimension(4) :: returnvalues
    do i=1,NUMRETURNPERIODS
        ! Get the return period from the array
        return_period = get_return_period(i)
        call sanitise_return_period(return_period)
    
        x = log10(return_period)
        if ( lwrite ) then
           print '(a,i2,a,f16.10)',"Calculating return levels for period (",i,") = 10 ** ",x
        endif

        ! Convert the return time from years to the series units (e.g. months)
        x = x + log10(real(j2-j1+1))
        ! Get the return levels for the period x
        call getreturnlevelsforperiod(a,b,xi,alpha,beta,cov1,cov2,cov3,covreturnlevel,j1,j2,assume,x,returnvalues)
        t(i,:) = returnvalues
    enddo
end subroutine getreturnlevels


subroutine getreturnyears(a,b,xi,alpha,beta,xyear,cov1,cov2,cov3,covreturnyear,j1,j2, &
        nblockyr,nblockens,tx,lchangesign,lwrite)

    use points_to_print_at
    implicit none
    integer :: j1,j2,nblockyr,nblockens
    real :: a,b,xi,alpha,beta,xyear,cov1,cov2,cov3,tx(4)
    logical :: lchangesign,lwrite
    real,external :: covreturnyear
    integer :: i
    !

    tx(1) = covreturnyear(a,b,xi,alpha,beta,xyear,cov1,lchangesign)
    tx(2) = covreturnyear(a,b,xi,alpha,beta,xyear,cov2,lchangesign)
    if ( cov3 < 1e33 ) then
        tx(4) = covreturnyear(a,b,xi,alpha,beta,xyear,cov3,lchangesign)
    else
        tx(4) = 3e33
    end if
    do i=1,4
        if ( i == 3 ) cycle
        if ( nblockyr > 1 .and. tx(i) < 5e19 ) tx(i) = tx(i)*nblockyr
        if ( nblockens > 1 .and. tx(i) < 5e19 ) tx(i) = tx(i)*nblockens
    end do
    if ( tx(2) > 1e19 ) then
        if ( tx(1) > 1e19 ) then
            tx(3) = 3e33
        else
            tx(3) = 1e20
        end if
    else
        if ( tx(1) > 1e19 ) then
            tx(3) = 1e20
        else
            tx(3) = tx(1) / tx(2)
        end if
    end if
    if ( lwrite ) then
        print *,'return time = ',tx
    end if
end subroutine getreturnyears


subroutine getabfromcov(a,b,alpha,beta,cov,aa,bb)
    implicit none
    real :: a,b,alpha,beta,cov,aa,bb
    real :: arg
    character cassume*5
    common /fitdata4/ cassume
    integer :: ncur
    real :: restrain
    logical :: llwrite,llchangesign
    common /fitdata2/ restrain,ncur,llwrite,llchangesign

    if ( alpha > 1e33 ) then
        aa = a
        bb = b
    else if ( cassume == 'shift' ) then
        aa = a + alpha*cov
        bb = b
    else if ( cassume == 'scale' ) then
        arg = alpha*cov/a
        if ( arg < 70 .and. a < 1e33 .and. b < 1e33 ) then
            aa = a*exp(alpha*cov/a)
            bb = b*aa/a
        else
            aa = 3e33
            bb = 3e33
        end if
    else if ( cassume == 'both' ) then
       aa = a + alpha*cov
       bb = b + beta*cov
    else
        write(0,*) 'getabfromcov: error: unknown value for assume: ',cassume
        call exit(-1)
    end if
     
end subroutine getabfromcov


subroutine printab(restrain,lnone,lweb)
    implicit none
    real,intent(in) :: restrain
    logical,intent(in) :: lnone,lweb
    character :: string*80
    character cassume*5
    common /fitdata4/ cassume

    string = ' '
    if ( restrain /= 0 ) then 
        if ( lweb ) then
            write(string,'(a,f4.1)') ' and a Gaussian penalty on &xi; of width 2&sigma; = ', &
                restrain
        else
            write(string,'(a,f4.1)') ' and a Gaussian penalty on xi of width 2sigma = ',restrain
        end if
    end if
    if ( lweb ) then
        if ( lnone ) then
            if ( string /= ' ' ) then
                print '(a)','# <tr><td colspan="4">'//trim(string)//'</td></tr>'
            endif
        else if ( cassume == 'shift' ) then
            print '(a)','# <tr><td colspan="4">'// &
                'with &mu;''= &mu;+&alpha;T '// &
            '   and &sigma;'' = &sigma;'//trim(string)//'</td></tr>'
        else if ( cassume == 'scale' ) then
            print '(a)','# <tr><td colspan="4">'// &
                'with &mu;'' = &mu; exp(&alpha;T/&mu;) and '// &
                '&sigma;'' = &sigma; exp(&alpha;T/&mu;)'//trim(string)//'</td></tr>'
        else if ( cassume == 'both' ) then
            print '(a)','# <tr><td colspan="4">'// &
                'with &mu;''= &mu;+&alpha;T '// &
                'and &sigma;'' = &sigma;+&beta;T'//trim(string)//'</td></tr>'
        else
            write(0,*) 'printab: error: unknow value for assume ',cassume
        end if
    else
        if ( lnone ) then
            if ( string /= ' ' ) then
                print '(a)',trim(string)
            end if
        else if ( cassume == 'shift' ) then
            print '(a)','# a'' = a+alpha*T, b''= b'//trim(string)
        else if ( cassume == 'scale' ) then
            print '(a)','# a'' = a*exp(alpha*T/a), b''= b*a''/a'//trim(string)
        else if ( cassume == 'both' ) then
            print '(a)','# a'' = a+alpha*T, b''= b+beta*T'//trim(string)
        else
            write(0,*) 'printab: error: unknow value for assume ',cassume
        end if
    end if
end subroutine printab

subroutine adjustyy(ntot,xx,assume,a,b,xi,alpha,beta,cov,yy,zz, &
    aaa,bbb,lchangesign,lwrite,covreturnyear,covreturnlevel)

    ! Adjusts the observations to produce predicted return levels yy (with related covariates zz)
    ! Also produces adjusted location and shape parameters aaa, bbb
    !   input: xx,assume,a,b,alpha,beta,cov,covreturnyear,covreturnlevel
    !   output: yy,zz,aaa,bbb
    !   flag: lwrite
    
    implicit none
    integer :: ntot
    real :: xx(2,ntot),yy(ntot),zz(ntot),yrs(5,ntot)
    real :: a,b,xi,alpha,beta,cov, rp, aaa, bbb
    character,intent(in) :: assume*(*)
    logical :: lchangesign,lwrite
    integer :: i
    real :: arg, xyear
    real,external :: covreturnyear
    real,external :: covreturnlevel
    
    ! Only use this for 'both' option where the return year/levels are needed
    ! Use default adjustyy if using shift or scale to keep existing behaviour
    if ( assume == 'both' ) then
        do i=1,ntot
            yy(i) = xx(1,i)
            zz(i) = xx(2,i)
            ! Get the return period rp for its own covariate (zz)
            ! Ref: covreturnyear(a,b,xi,alpha,beta,xyear,cov,lchangesign)
            !      where xyear is the target value
            ! Here we want to map cov -> zz
            rp = covreturnyear(a,b,xi,alpha,beta,yy(i),zz(i),lchangesign)
            
            ! Now get the adjusted return level
            ! Ref: gaucovreturnlevel(a,b,xi,alpha,beta,x,cov)
            ! Here we want to use the input cov value, not zz
            if (rp.gt.1d-30) then
               yy(i) = covreturnlevel(a,b,xi,alpha,beta,log10(rp),cov)
            endif
         enddo

         ! Get aaa, bbb (location and scale of adjusted distribution for cov)
         call getabfromcov(a,b,alpha,beta,cov,aaa,bbb)
    else
       ! Use regular method for adjusting yy for shift and scale (also gets aaa and bbb)
       call adjustyy_simple(ntot,xx,assume,a,b,alpha,beta,cov,yy,zz,aaa,bbb,lchangesign,lwrite)
    endif
end subroutine adjustyy

subroutine adjustyy_simple(ntot,xx,assume,a,b,alpha,beta,cov,yy,zz,aaa,bbb,lchangesign,lwrite)

!   Note: functions should call adjustyy to get correct 'both' behaviour
!   input: xx,assume,a,b,alpha,beta,cov
!   output: yy,zz,aaa,bbb
!   flag: lwrite

    implicit none
    integer :: ntot
    real :: xx(2,ntot),yy(ntot),zz(ntot)
    real :: a,b,alpha,beta,cov,aaa,bbb
    character,intent(in) :: assume*(*)
    logical :: lchangesign,lwrite
    integer :: i
    real :: arg

    !if ( lwrite ) then
    !    write(*,*) 'adjustyy: input ',assume
    !    print *,'a,b,alpha,beta,cov = ',a,b,alpha,beta,cov
    !    do i=1,min(5,ntot)
    !        print *,i,xx(1,i),xx(2,i)
    !    end do
    !end if
    do i=1,ntot
        yy(i) = xx(1,i)
        zz(i) = xx(2,i)
    end do
    if ( alpha > 1e33 ) then
        aaa = a
        bbb = b
    else if ( assume == 'shift' ) then
        do i=1,ntot
            yy(i) = yy(i) - alpha*(zz(i)-cov)
        end do
        aaa = a+cov*alpha
        bbb = b
    else if ( assume == 'scale' ) then
        do i=1,ntot
            arg = -alpha*(zz(i)-cov)/a
            if ( arg < 70 ) then
                yy(i) = yy(i)*exp(arg)
            else
                yy(i) = 3e33
            end if
        end do
        arg = alpha*cov/a
        if ( arg < 70 ) then
            aaa = a*exp(arg)
            bbb = b*exp(arg)
        else
            aaa = 3e33
            bbb = 3e33
        end if
    else if ( assume == 'both' ) then
        do i=1,ntot
            yy(i) = yy(i) - alpha*(zz(i)-cov)
        end do
        aaa = a + alpha*cov
        bbb = b + beta*cov
        !write(0,*) 'adjustyy: error: not yet ready'
        !write(*,*) 'adjustyy: error: not yet ready'
        !call exit(-1)

    else
        write(0,*) 'adjustyy_simple: error: unknown assumption not in list shift/scale/both'
        write(*,*) 'adjustyy_simple: error: unknown assumption not in list shift/scale/both'
        call exit(-1)
    end if
end subroutine adjustyy_simple


subroutine write_obscov(xx,yrs,ntot,xmin,cov2,xyear,year,offset,lchangesign)
    implicit none
    integer :: ntot,yrs(5,0:ntot),year
    real :: xx(2,ntot),xmin,cov2,xyear,offset
    logical :: lchangesign
    integer :: i,is
    logical :: lopen
    character string*1000,arg*250
    inquire(unit=15,opened=lopen)
    if ( lopen ) then
        if ( lchangesign ) then
            is = -1
        else
            is = +1
        end if
        string = ' '
        do i=0,command_argument_count()
            call get_command_argument(i,arg)
            string = trim(string)//' '//arg(index(arg,'/',.TRUE.)+1:)
        end do
        write(15,'(a)') '# ' // trim(string)
        write(15,'(a)') '# covariate  value'
        do i=1,ntot
            if ( xx(1,i) > xmin ) then
                write(15,'(2g20.6,i11)') xx(2,i)+offset,is*xx(1,i), &
                    10000*yrs(2,i)+100*yrs(3,i)+yrs(4,i)
            end if
        end do
        write(15,'(a)')
        write(15,'(a)')
        if ( xyear < 1e33 ) then
            write(15,'(2g20.6,i11)') cov2+offset,is*xyear,year
        else
            write(15,'(g20.6,a,i11)') cov2+offset,'-999.900',0
        end if
    end if
end subroutine write_obscov


subroutine write_threshold(cmin,cmax,a,b,xi,alpha,beta,offset,lchangesign,covreturnlevel)
    implicit none
    real :: cmin,cmax,a,b,xi,alpha,beta,offset
    real,external :: covreturnlevel
    logical :: lchangesign
    integer :: i,is
    real :: c,aa,bb,x6,x40,v6,v40
    logical :: lopen
    inquire(unit=15,opened=lopen) ! no flag in getopts.inc
    if ( lopen ) then
        if ( lchangesign ) then
            is = -1
        else
            is = +1
        end if
        write(15,'(a)')
        write(15,'(a)')
        write(15,'(a)') '# covariate threshold/position value(6yr) val(40yr)'
        do i=0,100
            c = cmin + (cmax-cmin)*i/100.
            call getabfromcov(a,b,alpha,beta,c,aa,bb)
            x6 = log10(6.)
            v6 = covreturnlevel(a,b,xi,alpha,beta,x6,c)
            x40 = log10(40.)
            v40 = covreturnlevel(a,b,xi,alpha,beta,x40,c)
            write(15,'(4g20.6)') c+offset,is*aa,is*v6,is*v40
        end do
    endif
end subroutine write_threshold


subroutine write_dthreshold(cov1,cov2,cov3,acov,offset,lchangesign)
    implicit none
    real :: cov1,cov2,cov3,acov(3,3),offset
    logical :: lchangesign
    integer :: i,j,is
    real :: a(3)
    logical :: lopen
    
    inquire(unit=15,opened=lopen) ! no flag in getopts.inc
    if ( lopen ) then
        is = +1 ! pre-flipped
        write(15,'(a)')
        write(15,'(a)')
        write(15,'(a)') '# covariate locationparameter/threshold lowerbound upperbound'
        do j=1,3
            if ( acov(j,1) < 1e33 ) then
                a(j) = is*acov(j,1)
            else
                a(j) = -999.9
            end if
        end do
        write(15,'(4g20.6)') cov1+offset,a
        do j=1,3
            if ( acov(j,2) < 1e33 ) then
                a(j) = is*acov(j,2)
            else
                a(j) = -999.9
            end if
        end do
        write(15,'(4g20.6)') cov2+offset,a
        if ( cov3 < 1e33 ) then
            do j=1,3
                if ( acov(j,3) < 1e33 ) then
                    a(j) = is*acov(j,3)
                else
                    a(j) = -999.9
                end if
            end do
            write(15,'(4g20.6)') cov3+offset,a
        end if
    endif
end subroutine write_dthreshold


subroutine write_residuals(yrseries,yrcovariate,nperyear,fyr,lyr,mens1,mens,assume, &
            a,b,xi,alpha,beta,lchangesign,xs,ntot,fit)
    implicit none
    integer,intent(in) :: nperyear,fyr,lyr,mens1,mens,ntot
    real,intent(in) :: yrseries(nperyear,fyr:lyr,0:mens),yrcovariate(nperyear,fyr:lyr,0:mens), &
        a,b,xi,alpha,beta
    real,intent(inout) :: xs(ntot)
    logical,intent(in) :: lchangesign
    character,intent(in) :: assume*(*),fit*(*)
    integer :: i,j,yr,mo,dy,month,is,iens,jens
    real :: aa,bb,f,s
    real,allocatable :: res(:,:,:)
    logical :: lopen
    integer,external :: find_x_in_array

    allocate(res(nperyear,fyr:lyr,0:mens))
    inquire(unit=16,opened=lopen)
    if ( lopen ) then
        if ( lchangesign ) then
            is = -1
        else
            is = +1
        end if
        do iens=mens1,mens
            do yr=fyr,lyr
                do mo=1,nperyear
                    if ( yrseries(mo,yr,iens) < 1e33 .and. yrcovariate(mo,yr,iens) < 1e33 ) then
                        call getabfromcov(a,b,alpha,beta,yrcovariate(mo,yr,iens),aa,bb)
                        j = find_x_in_array(yrseries(mo,yr,iens),xs,ntot) ! for date
                        if ( assume == 'scale' ) then
                            res(mo,yr,iens) = yrseries(mo,yr,iens)/aa - 1
                        else
                            res(mo,yr,iens) = yrseries(mo,yr,iens) - aa
                        end if
                        ! GPD fit is only valid above the threshold
                        if ( fit == 'gpd' .and. res(mo,yr,iens) < 0 ) res(mo,yr,iens) = 3e33
                    else
                        res(mo,yr,iens) = 3e33
                    end if
                end do
            end do
            if ( iens > mens1 ) write(16,'(a)')
            write(16,'(a,i6)') '# ensemble member ',iens
            write(16,'(a)')
            call printdatfile(16,res(1,fyr,iens),nperyear,nperyear,fyr,lyr)
        end do
        close(16)
    end if
end subroutine write_residuals

integer function find_x_in_array(x,xx,n)
    implicit none
    integer,intent(in) :: n
    real,intent(in) :: x
    real,intent(inout) :: xx(n)
    integer :: j
    do j=1,n
        if ( xx(j) == x ) exit
    end do
    if ( j > n ) then
        !!!write(0,*) 'find_x_in_array: internal error: cannot find x',x,' in linear array'
        j = 0
        return
    endif
    xx(j) = 3e33 ! so that data points are never counted twice
    find_x_in_array = j
end function find_x_in_array
