subroutine calcFreqDistr(a,qc,p10,p90)
      ! this routine calculates the 10th and 90th percentiles for a nwl length window 
      ! centered on each calender day in the period calyrbeg, calyrend
      implicit none
      include 'comgeneral_f90.h'

      integer       mxlength,mxperc
      parameter     (mxlength=(calyrend-calyrbeg+1)*nwl + 10,mxperc=5)
      real*8        a(yrbeg:yrend,12,31)
      integer       qc(yrbeg:yrend,12,31)
      real*8        p10(calyrbeg:calyrend+1,12,31)
      real*8        p90(calyrbeg:calyrend+1,12,31),dum(3*365)
      real*8        xdata(mxlength)
      integer       i,j,k,l,ii,jj,kk,m,n,length,ndata,npres
      integer       nwl2,absen,nperc
      integer       nthresh(mxperc)
      real*8        absenr,absentr,percentiles(mxperc)

      common/ndatablock/ndata
      common/xdatablock/xdata

      absen = absent + 1
      absenr = dble(absen)
      absentr = dble(absent)

      nwl2 = (nwl - 1)/2

      nperc = 2
      nthresh(1) = 10
      nthresh(2) = 90

      do j=1,12
        call lengthofmonth(i,j,length)
        do k=1,length

          if (zhang) then
            ! do the Zhang et al. approach: leave one year out
            do m=calyrbeg,calyrend

              npres = 0
              ndata = 0
              do i=calyrbeg,calyrend
                if(m.eq.i) goto 120

                ! count how many years are present
                if(qc(i,j,k).eq.0) npres=npres+1

                ! pool data from an nwl-length window
                do l=-nwl2,nwl2
                  call adjustdate(i,j,k,l,ii,jj,kk)
                  if(qc(ii,jj,kk).eq.0) then
                    ndata = ndata + 1
                    xdata(ndata) = a(ii,jj,kk)
                    !!!print *,'adding1 ',ndata,i,j,k,l,ii,jj,kk
                  endif
                enddo
 120            continue
              enddo
              ! add the extra year
              n = m+1
              if(n.gt.calyrend) n = calyrbeg
              if(qc(n,j,k).eq.0) npres=npres+1
              ! pool data from an nwl-length window
              do l=-nwl2,nwl2
                call adjustdate(n,j,k,l,ii,jj,kk)
                if(qc(ii,jj,kk).eq.0) then
                  ndata = ndata + 1
                  xdata(ndata) = a(ii,jj,kk)
                  !!!print *,'adding2 ',ndata,n,j,k,l,ii,jj,kk
                endif
              enddo

              ! if the amount of data does not equal or exceed the availability threshold: set percentiles to absent
              if(dble(npres)/dble(calyrend-calyrbeg+1).ge.percentage) then

                ! Use the Zhang et al. (2005) percentile system, linear interpolate between nearest data points
                call zhang2005perc(nperc,nthresh,percentiles)
                !call poormenperc(nperc,nthresh,percentiles)  

                p10(m,j,k) = percentiles(1)
                p90(m,j,k) = percentiles(2)
              else 
                p10(m,j,k) = absentr
                p90(m,j,k) = absentr
              endif

            enddo
          endif
          ! now calculate percentiles over the complete calibration period: no years left out
          m = calyrend+1
          ndata = 0
          do i=calyrbeg,calyrend
            ! pool data from an nwl-length window
            do l=-nwl2,nwl2
              call adjustdate(i,j,k,l,ii,jj,kk)
              if(qc(ii,jj,kk).eq.0) then
                ndata = ndata + 1
                xdata(ndata) = a(ii,jj,kk)
              endif
            enddo
          enddo

          ! if the amount of data does not equal or exceed the availability threshold: set percentiles to absent
          if(dble(npres)/dble(calyrend-calyrbeg+1).ge.percentage) then

           if (zhang) then
              call zhang2005perc(nperc,nthresh,percentiles)
              !call poormenperc(nperc,nthresh,percentiles)
           else
            if(.not.fancy) then
              ! use the "poor men's" percentile estimate: sort data and take the 10th & 90th percentile
              call poormenperc(nperc,nthresh,percentiles)
            else
              ! use the "fancy men's" percentile estimate: fit a three-parameter Gamma function
              call fancymenperc(nperc,nthresh,percentiles)
           endif
        endif

            ! Do simple check of percentiles
            if (percentiles(1).gt.percentiles(2)) write(*,*) "ERROR: 10th percentile is larger than 90th percentile:", &
                 & percentiles(1), percentiles(2)
            
            p10(m,j,k) = percentiles(1)
            p90(m,j,k) = percentiles(2)
          else
            p10(m,j,k) = absentr
            p90(m,j,k) = absentr
          endif

        enddo
      enddo

      ! there is a chance that not all calenderdays are filled with a value if the zhang approach
      ! is used. Fill these missing calender days with a polynomial approximation
      !     if(zhang) then
      !       call fillValue(p10)
      !       call fillValue(p90)
      !     endif

      ! if the Zhang et al. approach is NOT followed: then replace p10(calyrbeg:calyrend)
      ! with the value in p10(calyrend+1)
      if(.not.zhang) then
        do m=calyrbeg,calyrend
          do j=1,12
            call lengthofmonth(m,j,length)
            do k=1,length
              p10(m,j,k) = p10(calyrend+1,j,k)
              p90(m,j,k) = p90(calyrend+1,j,k)
            enddo
          enddo
        enddo
      endif

      ! one may want to smooth the percentiles
      !     call smoothPercentiles(p10)
      !     call smoothPercentiles(p90)

      ! Feb. 29th is always a problem: substitute the average between Feb 28th and March 1st
      do i=calyrbeg,calyrend+1
        if((p10(i,2,28).gt.absenr).and.(p10(i,3,1).gt.absenr))then
          p10(i,2,29) = (p10(i,2,28) + p10(i,3,1))/2.0d0
        else
          p10(i,2,29) = absentr
        endif

        if((p90(i,2,28).gt.absenr).and.(p90(i,3,1).gt.absenr))then
          p90(i,2,29) = (p90(i,2,28) + p90(i,3,1))/2.0d0
        else
          p90(i,2,29) = absentr
        endif
      enddo

 123  format(e14.4)

      ! test
      !     do m=calyrbeg,calyrend+1
      !       do j=1,12
      !         call lengthofmonth(i,j,length)
      !         do k=1,length
      !           write(6,234) m,j,k, p90(m,j,k)
      !         enddo
      !       enddo
      !     enddo

 234  format(I5,2I3,f10.2)

      return
      end
!
!---------------------------------------------------------------------------
!
      subroutine poormenperc(nperc,nthresh,percentiles)
      ! this routine calculates the 10th and 90th percentile of the distribution based
      ! on the data in the array xdum
      !
      ! here the simple approach is used
      use statistics

      implicit none
      include 'comgeneral_f90.h'

      integer       mxlength
      parameter     (mxlength=(calyrend-calyrbeg+1)*nwl + 10)
      integer       N,nperc
      real*8        xdata(mxlength)
      integer       i,j,k,nthresh(nperc),nthreshd(nperc)
      real*8        percentiles(nperc)
      ! NAG things
      integer       ifail,irank(mxlength),absen
      real*8        absenr,absentr
      external      m01daf

      common/ndatablock/N
      common/xdatablock/xdata

      absen = absent + 1
      absenr = dble(absen)
      absentr = dble(absent)

      if(N.ge.pdayspresent) then
      ! sort array xdum into ascending order
        ifail = 0
      ! Replace with built-in sort function
      !        call m01daf(xdata,1,N,'Ascending',irank,ifail)
        call sort_index(xdata,N,irank)
        
        do j=1,nperc
          nthreshd(j) = nint(dble(nthresh(j)*N)/100.0d0)

          percentiles(j) = xdata(irank(nthreshd(j)))
          
          !do i=1,N
          !  if(irank(i).eq.nthreshd(j)) percentiles(j) = xdata(i)
          !enddo
        enddo
      else
        do j=1,nperc
          percentiles(j) = absentr
        enddo
     endif

     ! Debug dump of data
     if (percentiles(1) .gt. percentiles(2)) then
        write(*,*) "DEBUG DUMP", N
        write(*,*) percentiles
        write(*,*) xdata(1:N)
        write(*,*) irank(1:N)
        write(*,*) xdata(irank(1:N))
        call exit(0)
     endif

      return
      end

!
!---------------------------------------------------------------------------
!
      subroutine zhang2005perc(nperc,nthresh,percentiles)
        ! This routine calculates percentiles following Equation 1 in Zhang et al. (2005)
        ! (see https://doi.org/10.1175/JCLI3366.1, based on Frich et al., 2002)
        ! This finds the values either side of the percentile and linearly interpolates them
        !
        ! Inputs:
        ! nperc - number of percentiles to compute (integer) 
        ! nthresh - values of percentages to find (e.g. 10, 90) (integer * nperc) 
        ! Outputs:
        ! percentiles - output percentiles (real*8 * nperc)
        use statistics
  
        implicit none
        include 'comgeneral_f90.h'
  
        integer       mxlength
        parameter     (mxlength=(calyrend-calyrbeg+1)*nwl + 10)
        integer       N,nperc
        real*8        xdata(mxlength)
        integer       i,j,k,nthresh(nperc)
        real*8        percentiles(nperc)
        integer       irank(mxlength),absen
        real*8        absenr,absentr
        ! Used in interpolating between the bounding values of a percentile
        real*8        f_percentile, f_to_lower, modified_percentile
        integer       lower_index,upper_index
        real*8        epsilon
        logical       climpact_compare
        real*8        climpact_fact
  
        common/ndatablock/N
        common/xdatablock/xdata
  
        absen = absent + 1
        absenr = dble(absen)
        absentr = dble(absent)

        ! Introduce a factor to account for machine precision
        epsilon = 4d-16
        climpact_compare = .false.
        climpact_fact = 1d0/3d0

        
        
        if(N.ge.pdayspresent) then
          ! sort array into ascending order
          call sort_index(xdata,N,irank)
          
          do j=1,nperc
            ! Get the percentile as a fraction
            f_percentile = dble(nthresh(j))/100.0d0

            ! Get the percentile as a position in the array
            modified_percentile = f_percentile*N
            ! If chosen, do the same as Climpact, who add a 1/3 factor modifying the calculated percentile
            ! Note that this is not mentioned by Zhang, so left optional
            if (climpact_compare) then
               modified_percentile = climpact_fact + f_percentile * (N + 1 - 2*climpact_fact) - 1
            endif
            
            ! For each percentile requested, get the indices bounding the value requested
            lower_index = floor(modified_percentile)

            ! Make sure lower_index is within bounds
            if (lower_index < 0) lower_index = 0
            ! Not N as upper_index needs to be valid too
            if (lower_index >= N-1) lower_index = N-1
            ! Set the upper index too
            upper_index = lower_index + 1
            ! Get the fractional distance of the percentile between the upper and lower index
            ! upper_index - lower_index is 1, so dividing is superfluous
            f_to_lower = dble(modified_percentile - lower_index) ! / dble(upper_index - lower_index)

            ! Now calculate the linear interpolation between them  
            ! If f_to_lower is 0, we sit on the lower percentile
            ! If f_to_upper is 1, we sit on the upper percentile
            percentiles(j) = xdata(irank(lower_index))*(1d0-f_to_lower) + & 
                           & xdata(irank(upper_index))*f_to_lower
            
          enddo
        else
          do j=1,nperc
            percentiles(j) = absentr
          enddo
       endif
  
       ! Debug dump of data
       if (percentiles(1) .gt. percentiles(2)) then
          write(*,*) "DEBUG DUMP"
          write(*,*) N, f_to_lower, f_percentile, lower_index, upper_index
          write(*,*) percentiles
          write(*,*) xdata(1:N)
          write(*,*) irank(1:N)
          write(*,*) xdata(irank(1:N))
          call exit(0)
       endif
  
        return
      end subroutine 
!
!---------------------------------------------------------------------------
!
module chisq_function
      ! Using a module to allow wrapping the test function in c bindings
      ! This allows it to be used by fgsl

      use, intrinsic :: iso_c_binding
      use fgsl
      implicit none
    
      contains
    
          function func(offset, params) bind(c)
    
          use statistics
          
          implicit none
          include 'comgeneral_f90.h'
    
          integer       mxlength
          parameter     (mxlength=(calyrend-calyrbeg+1)*nwl + 10)
          integer       nclass
          parameter     (nclass=40)
          integer       N
          real*8        xdata(mxlength),array(mxlength)
          external      g01aaf
          integer       i,ifail,ndf
          integer       ifreq(nclass)
          real*8        xd,D,alpha,beta,chisq,p
          real*8        cint(nclass),par(2),prob(nclass),eval(nclass),ffreq(nclass)
          real*8        offset,chisqi(nclass)
          real*8        wt(mxlength),xmean,s2,s3,s4,xmin,xmax,wtsum
    
          type(c_ptr), value :: params
    
          real*8        func
          external      g08cgf,g01aef
    
          common/ndatablock/N
          common/xdatablock/xdata
          common/xfreq/cint
          common/shape/xmean,alpha,beta
    
          ! shift the distribution
          call shift(N,offset,xdata,array)
    
          ! construct frequency distribution of shifted array
          ifail = 0
    
          ! call g01aef(N,nclass,array,1,cint,ifreq,xmin,xmax,ifail)
          ifreq = make_histogram(array,N,cint,nclass)
    
          !     write(6,*) 'shift: ',offset
          !     do i=1,nclass
          !       write(6,*) i,cint(i),ifreq(i)
          !     enddo
    
          xd = 0.0D0
          do i=1,N
            xd = xd + log(xdata(i) - offset)
          enddo
          D = log(xmean - offset) - xd/dble(N)
          alpha = (1.0 + sqrt(1.0 + 4*D/3.0))/(4.0*D)
          beta = (xmean - offset)/alpha
    
          !     write(6,*) 'alpha & beta: ',alpha, beta
    
          par(1) = alpha
          par(2) = beta
    
          ! do the goodness-of-fit test
          ifail = 1
          ! call g08cgf(nclass,ifreq,cint,'G',par,0,prob,chisq,p,ndf,
          !           eval,chisqi,ifail)
    
          ffreq = ifreq
          chisq = chisquare_gamma_evaluate(ffreq,cint,nclass,par)
    
          ! if ifail = 10, then we can go on
          if((ifail.ne.0).and.(ifail.ne.10)) then
            write(6,63) ifail
            stop
          endif
    
          !     do i=1,nclass
          !       write(6,61) i,cint(i),ifreq(i),eval(i),chisqi(i)
          !     enddo
          !     write(6,*)
    
          !     write(6,62) chisq
    
     61   format(I4,e14.4,I4,2e14.4)
     62   format('Chi squared: ',e14.4)
     63   format('ifail is: ',I4)
    
          func = chisq
    
        end function func
    
      end module
!
!---------------------------------------------------------------------------
!
      subroutine fancymenperc(nperc,nthresh,percentiles)
      ! this routine calculates the 10th and 90th percentile of the distribution based
      ! on the data in the array xdum
      !
      ! here the fancy approach is used
      use minimizer
      use statistics
      use chisq_function

      implicit none
      include 'comgeneral_f90.h'

      integer       mxlength
      parameter     (mxlength=(calyrend-calyrbeg+1)*nwl + 10)
      integer       N,nperc,absen
      real*8        absenr,absentr
      real*8        xdata(mxlength),array(mxlength)
      integer       i,j,k,nthresh(nperc)
      real*8        percentiles(nperc)
      ! NAG things
      integer       ifail,nclass
      ! external      g01aaf,g08cgf,g01aef,e04abf,g01fff
      real*8        g01fff,xmx,xmn,xthresh

      real*8        dint
      parameter     (nclass=40,dint=1.0)
      real*8        xmin,xmax,s2,s3,s4,wtsum,wt(mxlength),chisq
      real*8        eps,t,a,b
      real*8        cint(nclass)
      integer       ifreq(nclass)
      integer       maxcal
      integer       length,ndum,mdum,probdist(200)
      real*8        xd,x,alpha,beta,D,offset,xmean
      external      m01daf

      common/ndatablock/N
      common/xdatablock/xdata
      common/xfreq/cint
      common/shape/xmean,alpha,beta

      absen = absent + 1
      absenr = dble(absen)
      absentr = dble(absent)

      if(N.ge.pdayspresent) then

      ! fit a Gamma distribution to the data using maximum-likelihood
      ! theory is in Wilks p89
      !
      ! if the data is negatively skewed, then it should be multiplied with -1 to make
      ! it positively skewed.
      ! the gamma distribution assumes a positively skewed distribution
        ifail = 0
      !        call g01aaf(N,xdata,0,wt,xmean,s2,s3,s4,xmin,xmax,wtsum,ifail)
        call calculate_statistics(xdata,N,xmin,xmean,xmax,s2,s3,s4)

      ! flip the distribution if neccessary
        if(s3.lt.0.0) then
          call flip(N,xdata)
          xd = -xmin
          xmin = -xmax
          xmax = xd
          xmean = -xmean
        endif

      ! the NAG routine g08cgf requires the distribution to be on the positive real axis
      ! shift the distribution with a distance -xmin + 1
        call shift(N,xmin-1.0,xdata,array)
        do i=1,N
          xdata(i) = array(i)
        enddo
        xmean = xmean - (xmin-1.0)

      ! construct the class-boundaries
        do i=1,nclass
          cint(i) = (i-1)*dint
        enddo

      ! construct frequency distribution of array
      !       ifail = 0

      !       call g01aef(N,nclass,xdata,1,cint,ifreq,xmn,xmx,ifail)

      !       do i=1,nclass
      !         write(6,*) i,-(cint(i)+(xmin-1.0)),ifreq(i)
      !       enddo

        a = -1.0
        b = 0.99
        eps = 1.0D-6
        t = 1.0D-4
        x = 0.0
        maxcal = 50
        ifail = 0

        ! call e04abf(func,eps,t,a,b,maxcal,x,chisq)
        chisq = minimize(func,a,b)

        ! this calculates the percentiles
        do i=1,nperc
          xthresh = dble(nthresh(i))/100.0d0
          ifail = 0
          !percentiles(i) = g01fff(xthresh,alpha,beta,0.0,ifail)
          percentiles(i) = integrate_gamma_func(xthresh,alpha,beta)
          
        enddo
  
        ! transform the percentiles back to the original distribution
        ! if s3: flip it!
        if(s3.lt.0.0) then
          if(nperc.gt.2) call error('can only do this trick with two')
          b = percentiles(1)
          percentiles(1) = percentiles(2)
          percentiles(2) = b
        endif

        do i=1,nperc
          percentiles(i) = percentiles(i) + (xmin - 1.0D0) + x

          if(s3.lt.0.0) then
            percentiles(i) = -percentiles(i)
          endif
        enddo
      else
        do i=1,nperc
          percentiles(i) = absentr
        enddo
      endif

      !     write(6,59) vp10,vp90

 59   format('P10, P90: ',3f8.2)
 60   format('Skewness coefficient: ',e14.4)
 62   format('Chi squared: ',I5,e14.4)
 63   format('optimal shift: ',e14.4)
 65   format('no. iterations: ',I4)

      return
      end
!
!---------------------------------------------------------------------------
!
      subroutine flip(n,x)
      implicit none

      integer       i,n
      real*8        x(n)

      do i=1,n
        x(i) = -1.0*x(i)
      enddo

      return
      end
!
!---------------------------------------------------------------------------
!
      subroutine shift(n,offset,x,y)
      implicit none

      integer       i,n
      real*8        offset,x(n),y(n)

      do i=1,n
        y(i) = x(i) - offset
      enddo

      return
      end
!
!---------------------------------------------------------------------------
!
      subroutine fillValue_polynomial(perc)
      ! this routine fills any absent value in perc with value derived from
      ! neighboring values.
      !
      ! the procedure is:
      ! 1) check if there are absent values at all
      ! 2) if there is a year in perc with missing values:
      !    3) fit a polynomial through the year with the missing data
      !    4) evaluate the polynomial on the calender date with the missing data
      !
      use pynumerical

      implicit none
      include 'comgeneral_f90.h'

      integer       norder,M
      parameter     (norder=6,M=366)
      integer       absen,i,j,k,length,np,nq,ifail
      integer       npmin,npmax
      real*8        perc(calyrbeg:calyrend+1,12,31)
      real*8        absentr,absenr,xarg,fit
      logical       missing
      real*8        x(M),y(M),w(M),s(norder+1),a(norder+1,norder+1)
      real*8        ak(norder+1),work1(3*M),work2(2*(norder+1))
      !external      e02adf,e02aef

      absen = absent + 1
      absenr = dble(absen)
      absentr = dble(absent)


      ! check if there is a missing value
      do i=calyrbeg,calyrend+1
        missing  = .false.
        do j=1,12
          call lengthofmonth(i,j,length)
          do k=1,length
            if(perc(i,j,k).lt.absenr) missing = .true.
          enddo
        enddo

        ! if missing = .true., fit polynomial
        if(missing)then
          np = 0
          nq = 0
          do j=1,12
            call lengthofmonth(i,j,length)
            do k=1,length
              np = np + 1
              if(perc(i,j,k).gt.absenr) then
                nq = nq + 1
                x(nq) = dble(np)
                y(nq) = perc(i,j,k)
                w(nq) = 1.0d0
              endif
            enddo
          enddo

          ! fit polynomial
          ifail = 0
          write(6,*) 'nq: ',nq
          !call e02adf(nq,norder+1,norder+1,x,y,w,work1,work2,a,s,ifail)
          ! This saves the result of the polynomial internally
          call pyn_chebyshev_fit(x,y,norder,minval(x),maxval(x))
      
          ! fill perc with the evaluation of the polynomial fit
          npmax = np
          npmin = 1
          np = 0
          do j=1,12
            call lengthofmonth(i,j,length)
            do k=1,length
              np = np + 1
              if(perc(i,j,k).lt.absenr) then
                ifail = 0
                xarg = dble(np - npmin)/dble(npmax - npmin)
                !call e02aef(norder+1,ak,xarg,fit,ifail)
                fit = pyn_chebyshev_evaluate(xarg)
                perc(i,j,k) = fit
              endif
            enddo
          enddo
        endif
      enddo

      return
      end
