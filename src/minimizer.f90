! Find a minimum in a function between lower and upper limits

module minimizer

    contains

 function minimize(input_function,lower,upper)
    ! Find a minimum in a function between lower and upper limits

    use, intrinsic :: iso_c_binding
    use fgsl

    implicit none

    ! Function to find minimum in
    real(fgsl_double),external::input_function
    ! Lower and upper values to search between
    real(fgsl_double),intent(in)::lower,upper
    ! Output value
    real(kind=8)::minimize
    
    ! Internal variables for calculations and FGSL bridging
    real(fgsl_double)::minimum
    real(fgsl_double), parameter :: eps5 = 1.0d-6
    integer(fgsl_int), parameter :: itmax_root = 50
    real(fgsl_double) :: ra, xlo, xhi
    type(c_ptr) :: ptr
    type(fgsl_function) :: stdfunc
    type(fgsl_min_fminimizer) :: min_fslv
    integer(fgsl_int) :: status, i


    ! Set a guess for the minimum in the middle of the bounds
    minimum = 0.5*(lower+upper)

    ! Initialise minimisation function
    min_fslv = fgsl_min_fminimizer_alloc(fgsl_min_fminimizer_brent)

    stdfunc = fgsl_function_init(input_function, ptr)
    
    status = fgsl_min_fminimizer_set(min_fslv, stdfunc, &
        minimum, lower, upper)

    i = 0
    do
        i = i + 1
        status = fgsl_min_fminimizer_iterate(min_fslv)
        if (status /= fgsl_success .or. i > itmax_root) then
            exit
        end if
        ra = fgsl_min_fminimizer_x_minimum(min_fslv)
        xlo = fgsl_min_fminimizer_x_lower(min_fslv)
        xhi = fgsl_min_fminimizer_x_upper(min_fslv)
        status = fgsl_min_test_interval (xlo, xhi, eps5, 0.0_fgsl_double)
        if (status == fgsl_success) exit
    end do

    call fgsl_min_fminimizer_free(min_fslv)
    call fgsl_function_free(stdfunc)

    minimize = ra

end function minimize

end module minimizer
