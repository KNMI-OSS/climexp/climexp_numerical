! Wrapper module for numerical functionality implemented through Python
! This is a fallback in the case where open-source Fortran options cannot be found

module pynumerical
    ! Wrapper module for numerical functionality implemented through Python
    use forpy_mod
    implicit none

    public :: pyn_init, pyn_cleanup, pyn_chebyshev_fit, pyn_chebyshev_evaluate

    private

    ! Chebyshev fit module
    type(module_py) :: chebyshev_py

    ! Wrapper for generic double variable to pass to Python
    type(tuple) :: chebyshev_fit_args_py
    type(tuple) :: chebyshev_eval_args_py

contains

    subroutine pyn_init
        ! Initialise the pynumerical module
        ! Internal ==>
        integer :: ierror
        ! Initialise Python components
        ierror = forpy_initialize()
        ierror = import_py(chebyshev_py, "chebyshev")
        ! Initialise memory used by wrappers
        ! Numbers match the number of function inputs
        ierror = tuple_create(chebyshev_fit_args_py, 5)
        ierror = tuple_create(chebyshev_eval_args_py, 1)
        write(*,*) "Initialised pynumerical"
    end subroutine pyn_init

    subroutine pyn_cleanup
        ! Cleanup Python components on program exit
        call chebyshev_fit_args_py%destroy
        call chebyshev_eval_args_py%destroy
        call chebyshev_py%destroy
        
        call forpy_finalize
        write(*,*) "Cleaned up pynumerical"
    end subroutine pyn_cleanup

    subroutine pyn_chebyshev_fit(x_to_fit,y_to_fit,order,range_lower,range_upper)
        ! Initialise the chebyshev fit function
        ! Calling this will wipe the previous fit
        ! If you do not wish this to happen, the code should be updated 
        !     to allow instances of Chebyshev series
        ! Inputs ==>
        ! npoints - Number of points in data series to fit (integer)
        ! integer,intent(in)::npoints
        ! (x,y)_to_fit - Data series to fit (double)
        real(kind=8),asynchronous,intent(in)::x_to_fit(:),y_to_fit(:)
        ! order - Order of Chebyshev polynomial (integer)
        integer,intent(in)::order
        ! range_(lower, upper) - lower and upper bounds of range to evaluate over (double)
        real(kind=8),intent(in)::range_lower,range_upper
        ! Internal ==>
        ! Status flag for forpy
        integer :: ierror
        ! Wrappers for inputs to pyn_chebyshev_fit
        type(ndarray) :: xfit_py, yfit_py
        ! No function outputs

        ! Wrap Fortran arrays
        ierror = ndarray_create_nocopy(xfit_py, x_to_fit)
        ierror = ndarray_create_nocopy(yfit_py, y_to_fit)

        ! Set value to pass to Python
        ierror = chebyshev_fit_args_py%setitem(0, xfit_py)
        ierror = chebyshev_fit_args_py%setitem(1, yfit_py)
        ierror = chebyshev_fit_args_py%setitem(2, order)
        ierror = chebyshev_fit_args_py%setitem(3, range_lower)
        ierror = chebyshev_fit_args_py%setitem(4, range_upper)

        ! Run Python script to evaluate the function
        ierror = call_py_noret(chebyshev_py, "chebyshev_fit",chebyshev_fit_args_py)

        ! Fit stored in Python module, so no need to output anything

    end subroutine pyn_chebyshev_fit

    function pyn_chebyshev_evaluate(x_to_evaluate)
        ! Evaluate Chebyshev polynomial at position using polynomial set up in pyn_chebyshev_fit

        ! Inputs ==>
        ! x_to_evaluate - input value(s) to evaluate polynomial at (double)
        real(kind=8),intent(in) :: x_to_evaluate
        ! Outputs ==>
        ! Value at x_to_fit to output
        real(kind=8) :: pyn_chebyshev_evaluate
        ! Internal ==>
        ! Status flag for forpy
        integer :: ierror
        ! Intermediary foresult 
        type(object) :: result_py

        ! Set value to pass to Python
        ierror = chebyshev_eval_args_py%setitem(0, x_to_evaluate)
        ! Run Python script to evaluate the function
        ierror = call_py(result_py, chebyshev_py, "chebyshev_evaluate",chebyshev_eval_args_py)
        ! Cast result to Fortran variable
        ierror = cast_nonstrict(pyn_chebyshev_evaluate, result_py)
        
    end function pyn_chebyshev_evaluate
    
end module pynumerical
